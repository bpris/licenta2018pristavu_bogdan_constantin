package com.example.bogdan.cpnsimulator;


import com.example.bogdan.cpnsimulator.data.DataTypeObjects;
import com.example.bogdan.cpnsimulator.utils.ExpressionParser;
import com.example.bogdan.cpnsimulator.utils.ExpressionUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by bpristavu on 2/9/2018.
 */

public class ExpressionMathUtilsTest extends BaseTest {
    private String validExpressionWithInterrogation;
    private String validExpressionWithoutInterrogation;
    private String invalidExpressionWithInterrogation;
    private String invalidExpressionWithoutInterrogation;
    private String validExpressionWithConstantsOnly;

    private DataTypeObjects variables;
    private DataTypeObjects values;

    @Before
    @Override
    public void init() {
        this.validExpressionWithInterrogation = "x==c&&y==a?1'x+2'b:1'b+2'x";
        this.invalidExpressionWithInterrogation = "invalid expression";
        this.validExpressionWithoutInterrogation = "1'x+2'b+1'c+2'a+3'y";
        this.invalidExpressionWithoutInterrogation = "invalid ";
        this.validExpressionWithConstantsOnly = "1'a+3'b";

        variables = new DataTypeObjects();
        variables.add("x");
        variables.add("y");

        values = new DataTypeObjects();
        values.add("a");
        values.add("b");
        values.add("c");
    }

    @After
    @Override
    public void end() {
    }

    @Test
    public void testExtractInterrogationFromExpression() {
        assertEquals(ExpressionUtils.getInterrogationFromExpression(validExpressionWithInterrogation), "x==c&&y==a");
    }

    @Test
    public void testExtractThenStatementFromExpression() {
        assertEquals(ExpressionUtils.getThenStatementFromExpression(validExpressionWithInterrogation), "1'x+2'b");
    }

    @Test
    public void testStatementHasVariables() {
        assertEquals(false, ExpressionUtils.statementHasVariables(validExpressionWithoutInterrogation, variables));
        assertEquals(true, ExpressionUtils.statementHasVariables(validExpressionWithConstantsOnly, variables));
    }

    @Test
    public void testExtractElseStatementFromExpression() {
        assertEquals(ExpressionUtils.getElseStatementFromExpression(validExpressionWithInterrogation), "1'b+2'x");
    }

    @Test
    public void isStatementValid() {
        assertEquals(true, ExpressionUtils.isStatementValid(validExpressionWithoutInterrogation, values, variables));
        assertEquals(false, ExpressionUtils.isStatementValid(invalidExpressionWithoutInterrogation, values, variables));
    }

    @Test
    public void isInterrogationValid() {
        assertEquals(true, ExpressionUtils.isInterrogationValid(ExpressionUtils.getInterrogationFromExpression(validExpressionWithInterrogation)));
        assertEquals(false, ExpressionUtils.isInterrogationValid(invalidExpressionWithInterrogation));
    }

    @Test
    public void expressionHasInterrogation() {
        assertEquals(true, ExpressionUtils.isInterrogationValid(validExpressionWithInterrogation));
        assertEquals(false, ExpressionUtils.isInterrogationValid(validExpressionWithoutInterrogation));
    }

    @Test
    public void isExpressionValid() {
        assertEquals(true, ExpressionUtils.isExpressionValid(validExpressionWithInterrogation, values, variables));
        assertEquals(true, ExpressionUtils.isExpressionValid(validExpressionWithoutInterrogation, values, variables));
        assertEquals(false, ExpressionUtils.isExpressionValid(invalidExpressionWithInterrogation, values, variables));
        assertEquals(false, ExpressionUtils.isExpressionValid(invalidExpressionWithoutInterrogation, values, variables));
    }

    @Test
    public void areObjectsExtractedCorrectly() {
        assertEquals(values, ExpressionUtils.getSpecificObjectsFromInterrogation(validExpressionWithInterrogation, values));
        assertEquals(variables, ExpressionUtils.getSpecificObjectsFromInterrogation(validExpressionWithInterrogation, variables));
        assertEquals(values, ExpressionUtils.getSpecificObjectsFromStatement(validExpressionWithoutInterrogation, values));
        assertEquals(variables, ExpressionUtils.getSpecificObjectsFromStatement(validExpressionWithoutInterrogation, variables));
    }

    @Test
    public void testInputExpressions() {
        String statementWithoutVariables = "1'a+2'b+1'c";

        assertEquals(true, ExpressionUtils.isInputExpressionValid(validExpressionWithConstantsOnly, values, variables));
        assertEquals(false, ExpressionUtils.isInputExpressionValid(validExpressionWithInterrogation, values, variables));
        assertEquals(false, ExpressionUtils.isInputExpressionValid(validExpressionWithoutInterrogation, values, variables));
        assertEquals(true, ExpressionUtils.isInputExpressionValid(statementWithoutVariables, values, variables));
    }

    @Test
    public void evaluateExpression() {
        String validExpression = "x==c&&y==a||z==a?1'x+2'b:1'b+2'x";
        String[] variables = new String[]{"x", "y", "z"};
        String[] values1 = new String[]{"c", "b", "a"};
        String[] values2 = new String[]{"d", "b"};
        assertEquals(true, ExpressionParser.evaluate(ExpressionUtils.getInterrogationFromExpression(validExpression), variables, values1));
        //assertEquals(true, ExpressionParser.evaluate(ExpressionUtils.getInterrogationFromExpression(validExpressionWithInterrogation), variables, values1));
        //assertEquals(false, ExpressionParser.evaluate(ExpressionUtils.getInterrogationFromExpression(validExpressionWithInterrogation), variables, values2));
    }
}
