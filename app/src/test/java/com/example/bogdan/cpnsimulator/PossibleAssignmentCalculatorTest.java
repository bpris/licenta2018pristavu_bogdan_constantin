package com.example.bogdan.cpnsimulator;

import com.example.bogdan.cpnsimulator.data.DataTypeObjects;
import com.example.bogdan.cpnsimulator.utils.AssignmentCalculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by bpristavu on 2/12/2018.
 */

public class PossibleAssignmentCalculatorTest extends BaseTest {
    DataTypeObjects values;
    DataTypeObjects variables;

    Set<HashMap<String, String>> result;
    Map<String, String> r;

    @Before
    public void init() {
        values = new DataTypeObjects();
        variables = new DataTypeObjects();
        values.add("a", 2);
        values.add("b", 2);
        values.add("c", 4);
        values.add("d", 3);
        values.add("e", 1);
        variables.add("x", 2);
        variables.add("y", 1);
        variables.add("z", 4);

        result = new LinkedHashSet<>();

        HashMap<String, String> assignment = new HashMap<>();
        assignment.put("x", "a");
        assignment.put("y", "b");
        result.add(assignment);
        assignment = new HashMap<>();
        assignment.put("x", "b");
        assignment.put("y", "a");
        result.add(assignment);

        r = new HashMap<>();
        r.put("x", "a");
        r.put("y", "b");
    }

    @After
    public void end() {

    }

    @Test
    public void findAssignmentsTest() {
        assertEquals(result, PossibleAssignmentsCalculator.findAllAssignmentsForLine(variables.getObjects(), values.getObjects()));
    }

    @Test
    public void findMatchTest() {
        assertEquals(r, PossibleAssignmentsCalculator.findMatch(variables.getObjects(), values.getObjects()));
    }

    @Test
    public void testCartesianProduct() {
        Set<String> variables = new HashSet<>();
        Set<String> values = new HashSet<>();

        variables.add("x");
        variables.add("y");
        variables.add("z");

        values.add("a");
        values.add("b");
        values.add("c");
        values.add("d");
        values.add("e");

        //assertEquals(null, AssignmentCalculator.cartesianProduct(values, variables));
        assertEquals(null, AssignmentCalculator.cartesianProduct(0, AssignmentCalculator.cartesianProduct(values, variables).toArray(new Set[]{})));
    }

    @Test
    public void testCorrectAssignments() {
        Set<String> var = new HashSet<>();
        Set<String> val = new HashSet<>();

        var.add("x");
        var.add("y");
        var.add("z");

        val.add("a");
        val.add("b");
        val.add("c");
        val.add("d");
        val.add("e");

        assertEquals(result, AssignmentCalculator.calculate(
                values.getObjects(),
                variables.getObjects(),
                AssignmentCalculator.cartesianProduct(0, AssignmentCalculator.cartesianProduct(val, var).toArray(new Set[]{}))));

    }
}
