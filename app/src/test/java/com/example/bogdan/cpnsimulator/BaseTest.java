package com.example.bogdan.cpnsimulator;

import org.junit.After;
import org.junit.Before;

/**
 * Created by bpristavu on 2/12/2018.
 */

public abstract class BaseTest {

    @Before
    public abstract void init();

    @After
    public abstract void end();
}
