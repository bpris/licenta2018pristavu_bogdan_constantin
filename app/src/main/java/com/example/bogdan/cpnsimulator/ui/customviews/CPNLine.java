package com.example.bogdan.cpnsimulator.ui.customviews;

import android.graphics.Color;
import android.graphics.PointF;
import android.view.View;
import android.widget.EditText;

import com.example.bogdan.cpnsimulator.data.CPNExpression;
import com.example.bogdan.cpnsimulator.data.DataTypeObjects;
import com.example.bogdan.cpnsimulator.ui.MainActivity;
import com.example.bogdan.cpnsimulator.utils.ExpressionUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bogdan on 12.01.2018.
 */

public class CPNLine {
    private String expression;
    private PointF expressionLocation;

    private BaseEntity startView;
    private BaseEntity endView;

    private PointF start;
    private PointF end;

    private int[] startLocation;
    private int[] endLocation;

    private int[] arrows;
    private int toolbarHeight;

    private EditText expressionInput;
    private CPNExpression cpnExpression;

    private double angle;

    public static final String JSON_IS_START_LOCATION_KEY = "is_start_location";
    public static final String JSON_START_VIEW_KEY = "start_view";
    public static final String JSON_END_VIEW_KEY = "end_view";
    public static final String JSON_CPN_EXPRESSION_KEY = "cpn_expression";

    public CPNLine(BaseEntity startView, BaseEntity endView, int toolbarHeight) {
        start = new PointF();
        end = new PointF();
        expressionLocation = new PointF();

        this.startView = startView;
        this.endView = endView;

        this.toolbarHeight = toolbarHeight;

        startLocation = new int[2];
        endLocation = new int[2];
        arrows = new int[4];
        startView.getLocationOnScreen(startLocation);

        startLocation[0] += MainActivity.getHorizontalScrollDistance();
        startLocation[1] += MainActivity.getVerticalScrollDistance();

        endView.getLocationOnScreen(endLocation);

        endLocation[0] += MainActivity.getHorizontalScrollDistance();
        endLocation[1] += MainActivity.getVerticalScrollDistance();

        calculateLine();
    }

    private void calculateLine() {
        angle = Math.atan2(endLocation[1] - startLocation[1], endLocation[0] - startLocation[0]);
        double phi = Math.toRadians(40);
        double rho = angle + phi;
        if (Math.abs(startLocation[0] - endLocation[0]) > Math.abs(startLocation[1] - endLocation[1])) {
            //draw from the left or right side of the view
            if (startLocation[0] < endLocation[0]) {
                //draw from left to right
                start.x = startLocation[0] + startView.getWidth();
                start.y = startLocation[1] - toolbarHeight;

                end.x = endLocation[0];
                end.y = endLocation[1] - toolbarHeight;
            } else {
                //draw from right to left
                start.x = startLocation[0];
                start.y = startLocation[1] - toolbarHeight;

                end.x = endLocation[0] + startView.getWidth();
                end.y = endLocation[1] - toolbarHeight;
            }
        } else {
            //draw from the bottom or top side of the view
            if (startLocation[1] > endLocation[1]) {
                //draw from the top side of the view
                start.x = startLocation[0] + (startView.getWidth() / 2);
                start.y = startLocation[1] - toolbarHeight - startView.getHeight() / 2;

                end.x = endLocation[0] + (startView.getWidth() / 2);
                end.y = endLocation[1] - toolbarHeight + startView.getHeight() / 2;
            } else {
                //draw from the bottom side of the view
                start.x = startLocation[0] + (startView.getWidth() / 2);
                start.y = startLocation[1] - toolbarHeight + startView.getHeight() / 2;

                end.x = endLocation[0] + (startView.getWidth() / 2);
                end.y = endLocation[1] - toolbarHeight - startView.getHeight() / 2;

            }
        }

        arrows[0] = (int) (end.x - 20 * Math.cos(rho));
        arrows[1] = (int) (end.y - 20 * Math.sin(rho));
        arrows[2] = (int) (end.x - 20 * Math.cos(rho - (2 * phi)));
        arrows[3] = (int) (end.y - 20 * Math.sin(rho - (2 * phi)));

        setExpressionLocation();
    }

    private void setExpressionLocation() {
        if (Math.abs(startLocation[0] - endLocation[0]) > Math.abs(startLocation[1] - endLocation[1])) {
            if (startLocation[0] < endLocation[0]) {
                expressionLocation.x = arrows[0] - startView.getWidth() * 2;
                expressionLocation.y = arrows[1] + startView.getHeight() * 2 / 3;
            } else {
                expressionLocation.x = arrows[2];
                expressionLocation.y = arrows[3] - startView.getHeight() / 2;
            }
        } else {
            if (startLocation[1] > endLocation[1]) {
                expressionLocation.x = arrows[2] - startView.getWidth();
                expressionLocation.y = arrows[3];
            } else {
                expressionLocation.x = arrows[0] - startView.getWidth();
                expressionLocation.y = arrows[1] - startView.getHeight();
            }
        }
    }

    public PointF getStart() {
        return start;
    }

    public PointF getEnd() {
        return end;
    }

    public BaseEntity getStartView() {
        return startView;
    }

    public BaseEntity getEndView() {
        return endView;
    }

    public int getArrowX1() {
        return arrows[0];
    }

    public int getArrowX2() {
        return arrows[2];
    }

    public int getArrowY1() {
        return arrows[1];
    }

    public int getArrowY2() {
        return arrows[3];
    }

    public double getAngle() {
        return Math.toDegrees(angle);
    }

    public PointF getExpressionLocation() {
        return expressionLocation;
    }

    public void setExpressionLocation(PointF expressionLocation) {
        this.expressionLocation = expressionLocation;
    }

    public EditText getExpressionInput() {
        return expressionInput;
    }

    public void setExpressionInput(final EditText expressionInput, final String lastValue, final DataTypeObjects values, final DataTypeObjects variables) {
        this.expressionInput = expressionInput;
        cpnExpression = new CPNExpression(lastValue, values, variables);
        if (lastValue != null && !lastValue.isEmpty()) {
            this.expressionInput.setText(lastValue.replaceAll("\\s+", ""));
            this.expression = lastValue;

            if (endView instanceof Location) {
                if (ExpressionUtils.isExpressionValid(lastValue, values, variables)) {
                    expressionInput.setTextColor(Color.BLACK);
                } else {
                    expressionInput.setTextColor(Color.RED);
                }
            } else {
                if(ExpressionUtils.isInputExpressionValid(lastValue, values, variables)){
                    expressionInput.setTextColor(Color.BLACK);
                } else {
                    expressionInput.setTextColor(Color.RED);
                }
            }
        }
        this.expressionInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.hasFocus()) {
                    expression = expressionInput.getText().toString();
                    expression = expression.replaceAll("\\s+", "");
                    expressionInput.setText(expression);
                    if (cpnExpression == null) {
                        cpnExpression = new CPNExpression(expression, values, variables);
                    }
                    cpnExpression.setExpression(expression);
                    if (endView instanceof Location) {
                        if (ExpressionUtils.isExpressionValid(expression, values, variables)) {
                            expressionInput.setTextColor(Color.BLACK);
                        } else {
                            expressionInput.setTextColor(Color.RED);
                        }
                    } else {
                        if(ExpressionUtils.isInputExpressionValid(expression, values, variables)){
                            expressionInput.setTextColor(Color.BLACK);
                        } else {
                            expressionInput.setTextColor(Color.RED);
                        }
                    }
                } else {
                    expressionInput.setTextColor(Color.BLACK);
                }
            }
        });
    }

    public CPNExpression getCPNExpression() {
        return cpnExpression;
    }

    public void setCpnExpression(CPNExpression cpnExpression) {
        this.cpnExpression = cpnExpression;
    }

    public boolean isPointOnLine(PointF point) {
        return (point.x - start.x) / (end.x - start.x) == (point.y - start.y) / (end.y - start.y);
    }

    public double pointToLineDistance(PointF point) {
        double normalLength = Math.sqrt((end.x - start.x) * (end.x - start.x) + (end.y - start.y) * (end.y - start.y));
        return Math.abs((point.x - start.x) * (end.y - start.y) - (point.y - start.y) * (end.x - start.x)) / normalLength;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!CPNLine.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        CPNLine aux = (CPNLine) obj;
        return this.startView.equals(aux.startView) && this.endView.equals(aux.endView);
    }

    @Override
    public String toString() {
        return startView.getViewName().getText().toString() + " -> " + endView.getViewName().getText().toString();
    }

    public JSONObject toJSON() {
        JSONObject object = new JSONObject();
        try {
            object.accumulate(JSON_IS_START_LOCATION_KEY, startView instanceof Location);
            object.accumulate(JSON_START_VIEW_KEY, startView.toJSON());
            object.accumulate(JSON_END_VIEW_KEY, endView.toJSON());
            if (cpnExpression != null) {
                object.accumulate(JSON_CPN_EXPRESSION_KEY, cpnExpression.toJSON());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
