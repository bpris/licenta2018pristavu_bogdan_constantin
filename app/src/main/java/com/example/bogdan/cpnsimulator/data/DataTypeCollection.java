package com.example.bogdan.cpnsimulator.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bpristavu on 1/31/2018.
 */

public class DataTypeCollection {
    private HashMap<String, DataTypeObjects> types;
    private HashMap<String, DataTypeObjects> variables;

    public static final String JSON_TYPES_KEY = "types";
    public static final String JSON_VARIABLES_KEY = "variables";

    public DataTypeCollection() {
        types = new HashMap<>();
        variables = new HashMap<>();
    }

    public ArrayList<String> getDataTypeNames() {
        ArrayList<String> data = new ArrayList<>();
        data.addAll(types.keySet());
        return data;
    }

    public void addNewType(String typeName) {
        types.put(typeName, new DataTypeObjects());
        variables.put(typeName, new DataTypeObjects());
    }

    public void addNewConstants(String typeName, DataTypeObjects dataTypeObjects) {
        types.put(typeName, dataTypeObjects);
        if (variables.get(typeName) == null) {
            variables.put(typeName, new DataTypeObjects());
        }
    }

    public void addNewVariables(String typeName, DataTypeObjects dataTypeVariables) {
        if (types.get(typeName) == null) {
            types.put(typeName, new DataTypeObjects());
        }
        variables.put(typeName, dataTypeVariables);
    }

    public ArrayList<Object> getVariables(String typeName) {
        ArrayList<Object> variablesResult = new ArrayList<>();
        if (variables.get(typeName) != null) {
            variablesResult.addAll(variables.get(typeName).getObjects().keySet());
        }
        return variablesResult;
    }

    public DataTypeObjects getConstantsForKey(String typeName) {
        return types.get(typeName);
    }

    public DataTypeObjects getVariablesForKey(String typeName) {
        return variables.get(typeName);
    }

    public ArrayList<Object> getConstantsNames(String typeName) {
        ArrayList<Object> constantNameResult = new ArrayList<>();
        if (types.get(typeName) != null) {
            constantNameResult.addAll(types.get(typeName).getObjects().keySet());
        }
        return constantNameResult;
    }

    public ArrayList<Integer> getConstantsCount(String typeName) {
        ArrayList<Integer> constantCountResult = new ArrayList<>();
        for (Object constantName : types.get(typeName).getObjects().keySet()) {
            constantCountResult.add(types.get(typeName).getObjects().get(constantName));
        }
        return constantCountResult;
    }

    public HashMap<String, DataTypeObjects> getTypes() {
        return types;
    }

    public HashMap<String, DataTypeObjects> getVariables() {
        return variables;
    }

    public JSONObject toJSON() {
        JSONObject object = new JSONObject();
        try {
            for (String key : types.keySet()) {
                JSONObject type = new JSONObject();
                type.accumulate(JSON_TYPES_KEY, new JSONObject(types.get(key).getObjects()));
                type.accumulate(JSON_VARIABLES_KEY, new JSONObject(variables.get(key).getObjects()));
                object.accumulate(key, type);
            }
//            object.accumulate(JSON_TYPES_KEY, new JSONObject(types));
//            object.accumulate(JSON_VARIABLES_KEY, new JSONObject(variables));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
