package com.example.bogdan.cpnsimulator.ui.customviews;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.DataTypeCollection;
import com.example.bogdan.cpnsimulator.ui.MainActivity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class VariableInputForm extends LinearLayout {

    private Context context;
    private Button addNewVariable;
    private Button confirmInput;
    private LinearLayout inputZone;

    private ArrayList<EditText> variables;
    private ArrayList<Spinner> variablesDataTypes;

    private DataTypeCollection dataTypeCollection;

    public VariableInputForm(Context context) {
        super(context);
        this.context = context;
        initViews();
        initData();
    }

    public VariableInputForm(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initViews();
        initData();
    }

    public VariableInputForm(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initViews();
        initData();
    }

    public VariableInputForm(Context context, DataTypeCollection data) {
        super(context);
        this.dataTypeCollection = data;
        this.context = context;
        initViews();
        initData();
    }

    private void initViews() {
        inflateLayout();
        this.addNewVariable = findViewById(R.id.add_variable);
        this.confirmInput = findViewById(R.id.confirm_button);
        this.inputZone = findViewById(R.id.ll_input_zone);

        initListeners();
    }

    private void initData() {
        this.variables = new ArrayList<>();
        this.variablesDataTypes = new ArrayList<>();
        populateWithVariables();
    }

    private void inflateLayout() {
        inflate(context, R.layout.variable_input_form_layout, this);
    }

    private void initListeners() {
        addNewVariable.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                inflateVariable(null, null);
            }
        });

        confirmInput.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkVariablesNamesDuplicates() || checkNameAlreadyUsed()) {
                    showDuplicateDataTypeNamesDialog();
                } else {
                    saveVariables();
                    VariableInputForm.this.setVisibility(GONE);
                    View view = null;
                    if (inputZone.getChildCount() != 0) {
                        view = inputZone.getChildAt(0);
                    }
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (view != null) {
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    } else {
                        imm.hideSoftInputFromWindow(inputZone.getWindowToken(), 0);
                    }
                    ((MainActivity) context).hideVariableInputForm();
                }
            }
        });
    }

    private void inflateVariable(String variableName, String dataTypeName) {
        final LinearLayout input = (LinearLayout) inflate(context, R.layout.variable_row_layout, null);
        final EditText variableInput = input.findViewById(R.id.variable_input);
        final Spinner dataTypePicker = input.findViewById(R.id.data_type_picker);
        final Button removeVariable = input.findViewById(R.id.remove_variable);

        variables.add(variableInput);
        variablesDataTypes.add(dataTypePicker);

        variableInput.requestFocus();
        prepareSpinnerAdapter(dataTypePicker);

        if (variableName != null && !variableName.isEmpty()) {
            variableInput.setText(variableName);
        }
        if (dataTypeName != null && !dataTypeName.isEmpty()) {
            dataTypePicker.setSelection(dataTypeCollection.getDataTypeNames().indexOf(dataTypeName));
        }
        removeVariable.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                variables.remove(variableInput);
                variablesDataTypes.remove(dataTypePicker);
                inputZone.removeView(input);
                resetLayout();
            }
        });

        inputZone.addView(input);
    }

    private void prepareSpinnerAdapter(Spinner spinner) {
        ArrayList<String> data = dataTypeCollection.getDataTypeNames();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_dropdown_textview, data);
        spinner.setAdapter(dataAdapter);
    }

    private void populateWithVariables() {
        ArrayList<String> variableNames = new ArrayList<>();
        ArrayList<String> variablesDataTypes = new ArrayList<>();
        for (String s : dataTypeCollection.getDataTypeNames()) {
            for (String variable : dataTypeCollection.getVariables().get(s).getObjectNames()) {
                variableNames.add(variable);
                variablesDataTypes.add(s);
            }
        }

        for (int i = 0; i < variableNames.size(); i++) {
            inflateVariable(variableNames.get(i), variablesDataTypes.get(i));
        }
        resetLayout();
    }

    private void saveVariables() {
        for (int i = 0; i < variablesDataTypes.size(); i++) {
            dataTypeCollection.getVariables().get(variablesDataTypes.get(i).getSelectedItem().toString()).add(variables.get(i).getText().toString());
        }
    }

    private void showDuplicateDataTypeNamesDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("You are using the same variable name multiple times");
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    private boolean checkVariablesNamesDuplicates() {
        List<String> variablesNames = new ArrayList<>();
        for (EditText variables : variables) {
            variablesNames.add(variables.getText().toString());
        }
        Set<String> set = new HashSet<String>(variablesNames);
        if (set.size() != variablesNames.size()) {
            return true;
        }
        return false;
    }

    private boolean checkNameAlreadyUsed() {
        for(EditText variable : variables){
            String variableName = variable.getText().toString();
            for (String s : dataTypeCollection.getDataTypeNames()) {
                if (dataTypeCollection.getConstantsForKey(s).getObjectNames().contains(variableName)) {
                    return true;
                }
                if(dataTypeCollection.getDataTypeNames().contains(variableName)){
                    return true;
                }
            }
        }
        return false;
    }

    private void resetLayout() {
        invalidate();
        requestLayout();
    }

}
