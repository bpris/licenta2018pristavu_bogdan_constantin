package com.example.bogdan.cpnsimulator.utils;

/**
 * Created by Bogdan on 15.01.2018.
 */

public class Constants {

    public static final long DOUBLE_CLICK_DELAY_MILLS = 400;
    public static final long DELAY = 500;
    public static final String[] COMPARE_SIGN_LIST = {"<", ">", "<=", ">=", "==", "!="};
    public static final String LESS_SIGN = "<";
    public static final String GREATER_SIGN = ">";
    public static final String LESS_OR_EQUAL_SIGN = "<=";
    public static final String GREATER_OR_EQUAL_SIGN = ">=";
    public static final String EQUAL_SIGN = "==";
    public static final String NOT_EQUAL_SIGN = "!=";
    public static final String THEN_SIGN = "?";
    public static final String ELSE_SIGN = ":";
    public static final char MULTIPLY_SIGN = '*';
    public static final char CONCATENATE_SIGN = '+';
    public static final char AND_SIGN = '&';
    public static final char OR_SIGN = '|';
    public static final String TEST = "";

    public static final String SHARED_PREF_NAME = "saved_petri_nets";

    public static final String INTENT_KEY = "intent_key";

    public static final String JSON_LOCATIONS_KEY = "locations";
    public static final String JSON_TRANSITIONS_KEY = "transitions";
    public static final String JSON_LINES_KEY = "lines";
    public static final String JSON_DATA_COLLECTION_KEY = "dataCollection";
}
