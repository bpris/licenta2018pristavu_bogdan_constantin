package com.example.bogdan.cpnsimulator.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.CacheManager;
import com.example.bogdan.cpnsimulator.data.DataTypeCollection;
import com.example.bogdan.cpnsimulator.ui.customviews.CPNLine;
import com.example.bogdan.cpnsimulator.ui.customviews.Location;
import com.example.bogdan.cpnsimulator.ui.customviews.Transition;
import com.example.bogdan.cpnsimulator.utils.ExpressionUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by bpristavu on 2/1/2018.
 */

public class AlertDialogHandler {
    private static MainActivity mainActivity;

    public AlertDialogHandler(MainActivity activity) {
        mainActivity = activity;
    }

    public void showFileChoosing(Context context, Map<String, ?> fileNames) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select a file from the list");

        ArrayList<String> rows = new ArrayList<>();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
        rows.addAll(fileNames.keySet());
        arrayAdapter.addAll(rows);

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String fileName = arrayAdapter.getItem(which);
                String content = CacheManager.getDataFromFile(fileName);
                try {
                    mainActivity.loadNetwork(new JSONObject(content));
                    mainActivity.setLastFileName(fileName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    public void showFileNameInputDialog(Context context,
                                        final ArrayList<Location> locations,
                                        final ArrayList<Transition> transitions,
                                        final CopyOnWriteArrayList<CPNLine> lines,
                                        final DataTypeCollection data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Enter file name");
        final EditText input = new EditText(context);
        if (!mainActivity.getLastFileName().isEmpty()) {
            input.setText(mainActivity.getLastFileName());
        }
        builder.setView(input);

        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String fileName = input.getText().toString();
                CacheManager.saveFile(fileName, locations, transitions, lines, data);
            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    public static void showConfirmOverwriteFile(Context context, final String fileName,
                                                final ArrayList<Location> locations,
                                                final ArrayList<Transition> transitions,
                                                final CopyOnWriteArrayList<CPNLine> lines,
                                                final DataTypeCollection data) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("File already exists. Do you want to override it ?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        CacheManager.updateFile(fileName, locations, transitions, lines, data);
                        mainActivity.setLastFileName(fileName);
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    public void showDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();

    }

    public static void showInputDialog(final Context context, final Transition transition) {
        transition.setEditing(true);
        final EditText input = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getResources().getString(R.string.guard_expression_option) + " for transition " + transition.getName());
        builder.setView(input);
        if (transition.getGuardExpression() != null && !transition.getGuardExpression().isEmpty()) {
            input.setText(transition.getGuardExpression());
        }
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String expression = input.getText().toString();
                if (expression.isEmpty()) {
                    transition.onGuardExpressionSet("");
                    dialog.cancel();
                    transition.setEditing(false);
                } else {
                    if (ExpressionUtils.isInterrogationValid(expression)) {
                        transition.onGuardExpressionSet(expression);
                        dialog.cancel();
                    } else {
                        dialog.cancel();
                        showInvalidGuardExpressionMessage(context);
                    }
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                transition.setEditing(false);
            }
        });
        builder.show();
    }

    public void showListDialog(final Context context, final Set<Map<String, String>> assignments) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Possible assignments are :");

        ArrayList<String> rows = new ArrayList<>();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
        if (assignments != null && !assignments.isEmpty()) {
            for (Map<String, String> assignment : assignments) {
                StringBuilder row = new StringBuilder();
                for (String key : assignment.keySet()) {
                    row.append(key);
                    row.append("=");
                    row.append(assignment.get(key));
                    row.append(", ");
                }
                row.setLength(row.length() - 2);
                rows.add(row.toString());
            }
        }
        arrayAdapter.addAll(rows);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    public static void selectDataTypeForLocationDialog(final Context context, ArrayList<String> dataTypeNames, final Location location) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose one data type from the list:");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(dataTypeNames);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mainActivity.setDataTypeForLocation(location, strName);
            }
        });
        builder.show();
    }

    private static void showInvalidGuardExpressionMessage(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Guard expression is not valid");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public static void showNewNetConfirmScreen(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("All unsaved changes will be lost. Are you sure you want to proceed ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mainActivity.createNewNet();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    public static void showConfirmActionWithoutSave(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to leave the app without saving ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mainActivity.finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

}
