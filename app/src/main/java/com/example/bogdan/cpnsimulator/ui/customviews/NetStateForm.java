package com.example.bogdan.cpnsimulator.ui.customviews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bogdan.cpnsimulator.R;

import java.util.ArrayList;

public class NetStateForm extends LinearLayout {
    private ArrayList<Location> locations;
    private LinearLayout inputZone;
    private Context context;

    public NetStateForm(Context context) {
        super(context);
    }

    public NetStateForm(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public NetStateForm(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NetStateForm(Context context, ArrayList<Location> locations) {
        super(context);
        this.locations = locations;
        this.context = context;
        inflateLayout();
        inputZone = findViewById(R.id.ll_input_zone);
        populateWithData(context);
    }

    public void inflateLayout() {
        inflate(context, R.layout.net_state_layout, this);
    }

    private void populateWithData(Context context) {
        for (Location location : locations) {
            inflateRow(location);
        }
    }

    private void inflateRow(Location location) {
        final LinearLayout input = (LinearLayout) inflate(context, R.layout.net_state_row, null);
        TextView locationName = input.findViewById(R.id.location_name);
        LinearLayout multiset = input.findViewById(R.id.ll_multiset_rows);

        locationName.setText(location.getName());
        for (String s : location.getInitialMarking().getObjects().keySet()) {
            final TextView multisetRow = (TextView) inflate(context, R.layout.multiset_row, null);
            multisetRow.setText(s + " -> " + String.valueOf(location.getInitialMarking().getObjects().get(s)));
            multiset.addView(multisetRow);
        }
        inputZone.addView(input);
    }
}
