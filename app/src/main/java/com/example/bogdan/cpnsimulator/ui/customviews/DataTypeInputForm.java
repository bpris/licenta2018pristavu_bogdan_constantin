package com.example.bogdan.cpnsimulator.ui.customviews;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.DataTypeCollection;
import com.example.bogdan.cpnsimulator.data.DataTypeObjects;
import com.example.bogdan.cpnsimulator.ui.MainActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DataTypeInputForm extends LinearLayout {
    private Button addType;
    private Button confirmButton;
    private LinearLayout inputZone;

    private DataTypeCollection data;
    private Context context;

    private ArrayList<EditText> dataTypeNames;
    private ArrayList<Spinner> dataTypeValues;
    private ArrayList<CheckBox> areValuesInteger;

    private ArrayList<EditText> minIntegerInterval;
    private ArrayList<EditText> maxIntegerInterval;

    public DataTypeInputForm(Context context) {
        super(context);
        this.context = context;
    }

    public DataTypeInputForm(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public DataTypeInputForm(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    public DataTypeInputForm(Context context, DataTypeCollection data) {
        super(context);
        this.context = context;
        this.data = data;
        initViews();
        initData();
    }

    private void initViews() {
        inflateLayout();
        addType = findViewById(R.id.add_type);
        confirmButton = findViewById(R.id.confirm_button);
        inputZone = findViewById(R.id.ll_input_zone);
        initListeners();
    }

    private void initData() {
        dataTypeNames = new ArrayList<>();
        dataTypeValues = new ArrayList<>();
        areValuesInteger = new ArrayList<>();
        minIntegerInterval = new ArrayList<>();
        maxIntegerInterval = new ArrayList<>();

        populateWithData();
    }

    private void inflateLayout() {
        inflate(context, R.layout.data_type_input_form_layout, this);
    }

    private void initListeners() {
        addType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                inflateRow(null, null, false);
            }
        });

        confirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkDataTypeNamesDuplicates()) {
                    showDuplicateDataTypeNamesDialog();
                } else {
                    saveValues();
                    DataTypeInputForm.this.setVisibility(GONE);
                    View view = null;
                    if (inputZone.getChildCount() != 0) {
                        view = inputZone.getChildAt(0);
                    }
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (view != null) {
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    } else {
                        imm.hideSoftInputFromWindow(inputZone.getWindowToken(), 0);
                    }
                    ((MainActivity) context).hideConstantsInputForm();
                }
            }
        });
    }

    private void saveValues() {
        data = new DataTypeCollection();
        for (int i = 0; i < dataTypeNames.size(); i++) {
            data.addNewType(dataTypeNames.get(i).getText().toString());
        }

        for (int i = 0; i < areValuesInteger.size(); i++) {
            if (!areValuesInteger.get(i).isChecked()) {
                if (dataTypeValues.get(i).getAdapter() != null) {
                    for (int j = 0; j < dataTypeValues.get(i).getAdapter().getCount(); j++) {
                        data.getConstantsForKey(dataTypeNames.get(i).getText().toString()).add((String) dataTypeValues.get(i).getAdapter().getItem(j));
                    }
                }
            } else {
                for (int j = Integer.valueOf(minIntegerInterval.get(i).getText().toString()); j <=
                        Integer.valueOf(maxIntegerInterval.get(i).getText().toString()); j++) {
                    data.getConstantsForKey(dataTypeNames.get(i).getText().toString()).add(String.valueOf(j));
                }
            }
        }
        ((MainActivity) context).setDataTypeCollection(data);
    }

    private void populateWithData() {
        ArrayList<String> dataTypeValues = new ArrayList<>();
        for (String s : data.getDataTypeNames()) {
            if (!data.getTypes().get(s).getObjectNames().isEmpty()) {
                for (String value : data.getTypes().get(s).getObjectNames()) {
                    if (isNumeric(value)) {
                        ArrayList<Integer> temp = getIntegerArray(data.getTypes().get(s).getObjectNames());
                        dataTypeValues.add(String.valueOf(Collections.min(temp)));
                        dataTypeValues.add(String.valueOf(Collections.max(temp)));
                        inflateRow(s, dataTypeValues, true);
                        dataTypeValues.clear();
                        break;
                    } else {
                        dataTypeValues.add(value);
                    }
                }
                if (!dataTypeValues.isEmpty() && !isNumeric(dataTypeValues.get(0))) {
                    inflateRow(s, dataTypeValues, false);
                    dataTypeValues.clear();
                }
            } else {
                inflateRow(s, null, false);
            }
        }
    }

    private void inflateRow(final String dataTypeName, ArrayList<String> values, final boolean isInteger) {
        final LinearLayout input = (LinearLayout) inflate(context, R.layout.data_type_row_layout, null);
        final LinearLayout stringInput = input.findViewById(R.id.string_input_row);
        final LinearLayout integerInput = input.findViewById(R.id.integer_input_row);
        final EditText dataTypeNameInput = input.findViewById(R.id.data_type_name);
        final Spinner valuesForDataType = input.findViewById(R.id.added_values);
        final Button addValueForDataType = input.findViewById(R.id.btn_add_value);
        final EditText minIntegerInput = input.findViewById(R.id.min_interval);
        final EditText maxIntegerInput = input.findViewById(R.id.max_interval);
        final Button removeRow = input.findViewById(R.id.remove_row);
        final CheckBox inputCheckBox = input.findViewById(R.id.integer_checkbox);

        dataTypeNames.add(dataTypeNameInput);
        dataTypeValues.add(valuesForDataType);
        minIntegerInterval.add(minIntegerInput);
        maxIntegerInterval.add(maxIntegerInput);
        areValuesInteger.add(inputCheckBox);

        if (isInteger) {
            integerInput.setVisibility(VISIBLE);
            stringInput.setVisibility(GONE);
        } else {
            integerInput.setVisibility(GONE);
            stringInput.setVisibility(VISIBLE);
        }

        inputCheckBox.setChecked(isInteger);

        if (dataTypeName != null && !dataTypeName.isEmpty()) {
            dataTypeNameInput.setText(dataTypeName);
            if (isInteger) {
                minIntegerInput.setText(values.get(0));
                maxIntegerInput.setText(values.get(1));
            } else {
                if (values != null && !values.isEmpty()) {
                    prepareSpinnerAdapter(valuesForDataType, values);
                }
            }
        } else {
            dataTypeNameInput.setText("type name");
        }

        inputCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    integerInput.setVisibility(VISIBLE);
                    stringInput.setVisibility(GONE);
                } else {
                    integerInput.setVisibility(GONE);
                    stringInput.setVisibility(VISIBLE);
                }
            }
        });

        addValueForDataType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogForValueName(dataTypeNameInput.getText().toString(), valuesForDataType);
            }
        });

        removeRow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTypeNames.remove(dataTypeNameInput);
                dataTypeValues.remove(valuesForDataType);
                minIntegerInterval.remove(minIntegerInput);
                maxIntegerInterval.remove(maxIntegerInput);
                areValuesInteger.remove(inputCheckBox);
                inputZone.removeView(input);
                resetLayout();
            }
        });

        inputZone.addView(input);
    }

    private void prepareSpinnerAdapter(Spinner spinner, ArrayList<String> values) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_dropdown_textview, new ArrayList<String>());
        if (values != null && !values.isEmpty()) {
            dataAdapter.addAll(values);
            dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_textview);
            spinner.setAdapter(dataAdapter);
        }
    }

    private void resetLayout() {
        invalidate();
        requestLayout();
    }

    private boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private ArrayList<Integer> getIntegerArray(ArrayList<String> stringArray) {
        ArrayList<Integer> result = new ArrayList<>();
        for (String stringValue : stringArray) {
            try {
                //Convert String to Integer, and store it into integer array list.
                result.add(Integer.parseInt(stringValue));
            } catch (NumberFormatException nfe) {
                //System.out.println("Could not parse " + nfe);
            }
        }
        return result;
    }

    private void showDialogForValueName(final String dataTypeName, final Spinner spinner) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Add a new value for " + dataTypeName);
        final EditText input = new EditText(context);
        builder.setView(input);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String valueName = input.getText().toString();
                if (valueName.isEmpty()) {
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                    builderInner.setMessage("Please insert a value in the editable field");
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            showDialogForValueName(dataTypeName, spinner);
                        }
                    });
                    builderInner.show();
                } else if (isNumeric(valueName)) {
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                    builderInner.setMessage("If you want to insert numbers, please check the 'interger' checkbox");
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                            showDialogForValueName(dataTypeName, spinner);
                        }
                    });
                    builderInner.show();
                } else if (checkNameAlreadyUsed(valueName)) {
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                    builderInner.setMessage("The name is already being used");
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            showDialogForValueName(dataTypeName, spinner);
                        }
                    });
                    builderInner.show();
                } else {
                    if (data.getConstantsForKey(dataTypeName) == null) {
                        data.addNewConstants(dataTypeName, new DataTypeObjects());
                    }
                    data.getConstantsForKey(dataTypeName).add(input.getText().toString());
                    prepareSpinnerAdapter(
                            spinner,
                            data.getConstantsForKey(dataTypeName).getObjectNames());
                }
            }
        });
        builder.show();
    }

    private void showDuplicateDataTypeNamesDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("You are using the same name multiple times");
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    private boolean checkNameAlreadyUsed(String valueName) {
        for (String s : data.getDataTypeNames()) {
            if (data.getConstantsForKey(s).getObjectNames().contains(valueName)) {
                return true;
            }
            if (data.getVariablesForKey(s).getObjectNames().contains(valueName)) {
                return true;
            }
        }
        return data.getDataTypeNames().contains(valueName);
    }

    private boolean checkDataTypeNamesDuplicates() {
        List<String> typeNames = new ArrayList<>();
        for (EditText dataTypename : dataTypeNames) {
            typeNames.add(dataTypename.getText().toString());
        }
        Set<String> set = new HashSet<String>(typeNames);
        if (set.size() != typeNames.size()) {
            return true;
        }
        return false;
    }
}
