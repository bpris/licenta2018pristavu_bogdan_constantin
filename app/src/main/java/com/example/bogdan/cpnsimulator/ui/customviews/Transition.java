package com.example.bogdan.cpnsimulator.ui.customviews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.ui.AlertDialogHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Bogdan on 12.01.2018.
 */

public class Transition extends BaseEntity {
    private String guardExpression;

    public static final String JSON_GUARD_EXPRESSION_KEY = "guard_expression";

    public Transition(@NonNull Context context) {
        super(context);
    }

    public Transition(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Transition(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public Transition(Context context, String name) {
        super(context, name);
    }

    @Override
    protected void inflateLayout() {
        inflate(getContext(), R.layout.square_view, this);
    }

    @Override
    public TextView getViewName() {
        return findViewById(R.id.transition_name);
    }

    @Override
    public EditText getEditViewName() {
        return findViewById(R.id.edit_transition_name);
    }

    @Override
    public void forceCloseAll() {
        super.forceCloseAll();
    }

    public void guardExpression() {
        this.setEditing(true);
        AlertDialogHandler.showInputDialog(getContext(), this);
    }

    public void onGuardExpressionSet(String guardExpression) {
        this.setEditing(false);
        this.guardExpression = guardExpression;
    }

    public String getGuardExpression() {
        return guardExpression;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject object = new JSONObject();
        try {
            object.accumulate(JSON_NAME_KEY, getName());
            object.accumulate(JSON_X_COORDINATE, this.getX());
            object.accumulate(JSON_Y_COORDINATE, this.getY());
            if (this.guardExpression != null) {
                object.accumulate(JSON_GUARD_EXPRESSION_KEY, this.guardExpression);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
