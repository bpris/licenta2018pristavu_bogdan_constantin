package com.example.bogdan.cpnsimulator.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class GuardExpressionUtils {

    private static String STATEMENT_REGEX = "[\\d+'[a-z]+\\d*[a-z]*]+";
    private static String LOWER_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*<[a-z]+\\d*[a-z]*";
    private static String LOWER_OR_EQUAL_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*<=[a-z]+\\d*[a-z]*";
    private static String GREATER_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*>[a-z]+\\d*[a-z]*";
    private static String GREATER_OR_EQUAL_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*<[a-z]+\\d*[a-z]*";
    private static String EQUAL_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*==[a-z]+\\d*[a-z]*";
    private static String NOT_EQUAL_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*!=[a-z]+\\d*[a-z]*";


    public static Set<Map<String, String>> getPossibleAssignment(String guardExpression) {
        StringBuilder subExpression = new StringBuilder();
        if (guardExpression == null || guardExpression.isEmpty()) {
            return null;
        }
        Set<Map<String, String>> possibleAssignments = new HashSet<>();

        if (!guardExpression.contains("&") && !guardExpression.contains("|")) {
            if (guardExpression.matches(EQUAL_INTERROGATION_REGEX)) {
                String key = guardExpression.substring(0, guardExpression.indexOf('='));
                String value = guardExpression.substring(
                        guardExpression.indexOf('=') + 2, guardExpression.length());
                Map<String, String> assignment = new HashMap<>();
                assignment.put(key, value);
                possibleAssignments.add(assignment);
            }
        } else {
            for (int i = 0; i < guardExpression.length(); i++) {
                char ch = guardExpression.charAt(i);
                if (ch != '&' && ch != '|') {
                    subExpression.append(ch);
                } else {
                    subExpression = new StringBuilder();
                }
                if (subExpression.toString().matches(EQUAL_INTERROGATION_REGEX)) {
                    String key = subExpression.toString().substring(
                            0,
                            subExpression.toString().indexOf('='));
                    String value = subExpression.toString().substring(
                            subExpression.toString().indexOf('=') + 2,
                            subExpression.length());
                    Map<String, String> assignment = new TreeMap<>();
                    assignment.put(key, value);
                    possibleAssignments.add(assignment);
                    subExpression = new StringBuilder();
                }
            }
        }
        return possibleAssignments;
    }

    public static Set<Map<String, String>> getNotPossibleAssignment(String guardExpression) {
        StringBuilder subExpression = new StringBuilder();
        if (guardExpression == null || guardExpression.isEmpty()) {
            return null;
        }
        Set<Map<String, String>> possibleAssignments = new HashSet<>();

        if (!guardExpression.contains("&") && !guardExpression.contains("|")) {
            if (guardExpression.matches(NOT_EQUAL_INTERROGATION_REGEX)) {
                String key = guardExpression.substring(0, guardExpression.indexOf('!'));
                String value = guardExpression.substring(
                        guardExpression.indexOf('=') + 1,
                        guardExpression.length());
                Map<String, String> assignment = new HashMap<>();
                assignment.put(key, value);
                possibleAssignments.add(assignment);
            }
        } else
            for (int i = 0; i < guardExpression.length(); i++) {
                char ch = guardExpression.charAt(i);
                if (ch != '&' && ch != '|') {
                    subExpression.append(ch);
                } else {
                    subExpression = new StringBuilder();
                }
                if (subExpression.toString().matches(NOT_EQUAL_INTERROGATION_REGEX)) {
                    String key = subExpression.toString().substring(
                            0,
                            subExpression.toString().indexOf('!'));
                    String value = subExpression.toString().substring(
                            subExpression.toString().indexOf('!') + 1,
                            subExpression.length());
                    Map<String, String> assignment = new TreeMap<>();
                    assignment.put(key, value);
                    possibleAssignments.add(assignment);
                    subExpression = new StringBuilder();
                }
            }
        return possibleAssignments;
    }
}
