package com.example.bogdan.cpnsimulator.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.bogdan.cpnsimulator.R;

import java.util.ArrayList;

/**
 * Created by bpristavu on 1/16/2018.
 */

public class OptionListAdapter extends BaseAdapter {
    private int totalHeight;
    private Context context;
    private ArrayList<String> options;

    public OptionListAdapter(Context context, ArrayList<String> options) {
        this.context = context;
        this.options = options;
        totalHeight = 0;
    }

    @Override
    public int getCount() {
        return options.size();
    }

    @Override
    public Object getItem(int i) {
        return options.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.option_list_item, viewGroup, false);
            holder.optionName = view.findViewById(R.id.option_name);
            holder.separator = view.findViewById(R.id.separator);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.optionName.setText(options.get(i));
        if (getCount() == i + 1) {
            holder.separator.setVisibility(View.INVISIBLE);
        } else {
            holder.separator.setVisibility(View.VISIBLE);
        }
        return view;
    }

    static class ViewHolder {
        TextView optionName;
        View separator;
    }
}
