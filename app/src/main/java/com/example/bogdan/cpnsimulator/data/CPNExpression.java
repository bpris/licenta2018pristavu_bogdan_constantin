package com.example.bogdan.cpnsimulator.data;

import com.example.bogdan.cpnsimulator.utils.ExpressionParser;
import com.example.bogdan.cpnsimulator.utils.ExpressionUtils;
import com.example.bogdan.cpnsimulator.utils.AssignmentCalculator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Bogdan on 12.02.2018.
 */

public class CPNExpression {
    private String expression;
    private DataTypeObjects values;
    private DataTypeObjects variables;
    private Map<String, String> lastAssignment;

    public static final String JSON_EXPRESSION_KEY = "expression";
    public static final String JSON_VALUES_KEY = "values";
    public static final String JSON_VARIABLES_KEY = "variables";

    public CPNExpression(String expression, DataTypeObjects values, DataTypeObjects variables) {
        this.expression = expression;
        this.values = values;
        this.variables = variables;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public DataTypeObjects getValues() {
        return values;
    }

    public DataTypeObjects getVariables() {
        return variables;
    }

    public void setValues(DataTypeObjects values) {
        this.values = values;
    }

    public void setVariables(DataTypeObjects variables) {
        this.variables = variables;
    }

    public Map<String, String> getLastAssignment() {
        return lastAssignment;
    }

    public void setLastAssignment(Map<String, String> lastAssignment) {
        this.lastAssignment = lastAssignment;
    }

    public Set<Map<String, String>> getAssignmentsForLine() {
        String statement;
        if (expression == null) {
            return null;
        }
        if (!ExpressionUtils.isExpressionValid(expression, values, variables)) {
            return null;
        }
        if (!ExpressionUtils.hasInterrogation(expression)) {
            statement = expression;
        } else {
            ArrayList<String> tempValues = new ArrayList<>();
            ArrayList<String> tempVariables = new ArrayList<>();
            if (lastAssignment != null) {
                for (String s : lastAssignment.keySet()) {
                    tempValues.add(s);
                    tempVariables.add(lastAssignment.get(s));
                }
            }
            if (tempValues.size() == 0) {
                if (ExpressionParser.evaluate(
                        expression,
                        variables.getObjects().keySet().toArray(new String[variables.getObjects().size()]),
                        values.getObjects().keySet().toArray(new String[values.getObjects().size()]))) {
                    statement = ExpressionUtils.getThenStatementFromExpression(expression);
                } else {
                    statement = ExpressionUtils.getElseStatementFromExpression(expression);
                }
            } else if (ExpressionParser.evaluate(
                    expression,
                    tempVariables.toArray(new String[tempVariables.size()]),
                    tempValues.toArray(new String[tempValues.size()]))) {
                statement = ExpressionUtils.getThenStatementFromExpression(expression);
            } else {
                statement = ExpressionUtils.getElseStatementFromExpression(expression);
            }
        }

        if (ExpressionUtils.statementHasConstants(statement, values) && ExpressionUtils.statementHasVariables(statement, variables)) {
            Set<Map<String, String>> result = AssignmentCalculator.calculate(
                    values.getObjects(),
                    ExpressionUtils.getSpecificObjectsFromStatement(statement, variables).getObjects(),
                    AssignmentCalculator.cartesianProduct(
                            0,
                            AssignmentCalculator.cartesianProduct(
                                    values.getObjects().keySet(),
                                    ExpressionUtils.getSpecificObjectsFromStatement(statement, variables).getObjects().keySet()).toArray(new Set[]{})));
            DataTypeObjects tempvalues = ExpressionUtils.getSpecificObjectsFromStatement(statement, values);
            Map<String, String> assignment = new LinkedHashMap<>();
            for (int i = 0; i < tempvalues.getObjects().size(); i++) {
                assignment.put("v" + (int) (i + 1), tempvalues.getObjectNames().get(i));
            }

            for (Map<String, String> possibleAssignment : result) {
                possibleAssignment.putAll(assignment);
            }
            return result;
        }

        if (!ExpressionUtils.statementHasVariables(statement, variables)) {
            DataTypeObjects tempvalues = ExpressionUtils.getSpecificObjectsFromStatement(statement, values);
            Set<Map<String, String>> result = new LinkedHashSet<>();
            Map<String, String> assignment = new LinkedHashMap<>();
            for (int i = 0; i < tempvalues.getObjects().size(); i++) {
                assignment.put("v" + (int) (i + 1), tempvalues.getObjectNames().get(i));
            }
            result.add(assignment);
            return result;
        }

        if (!ExpressionUtils.statementHasConstants(statement, values)) {
            return AssignmentCalculator.calculate(
                    values.getObjects(),
                    ExpressionUtils.getSpecificObjectsFromStatement(statement, variables).getObjects(),
                    AssignmentCalculator.cartesianProduct(
                            0,
                            AssignmentCalculator.cartesianProduct(
                                    values.getObjects().keySet(),
                                    ExpressionUtils.getSpecificObjectsFromStatement(statement, variables).getObjects().keySet()).toArray(new Set[]{})));
        }

        return Collections.emptySet();
    }

    public JSONObject toJSON() {
        JSONObject object = new JSONObject();
        try {
            object.accumulate(JSON_EXPRESSION_KEY, expression);
            if (values != null) {
                object.accumulate(JSON_VALUES_KEY, new JSONObject(values.getObjects()));
            }

            if (variables != null) {
                object.accumulate(JSON_VARIABLES_KEY, new JSONObject(variables.getObjects()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
