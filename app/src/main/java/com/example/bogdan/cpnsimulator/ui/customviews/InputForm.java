package com.example.bogdan.cpnsimulator.ui.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.DataTypeCollection;
import com.example.bogdan.cpnsimulator.data.DataTypeObjects;
import com.example.bogdan.cpnsimulator.ui.AlertDialogHandler;
import com.example.bogdan.cpnsimulator.ui.MainActivity;
import com.example.bogdan.cpnsimulator.utils.DataTypesValidator;

import java.util.ArrayList;

/**
 * Created by bpristavu on 1/31/2018.
 */

public class InputForm extends LinearLayout {
    private Context context;
    private TextView addField;
    private TextView finishAdding;
    private TextView cancelButton;
    private LinearLayout inputOptions;
    private EditText dataTypeName;
    private Spinner spinnerDataTypes;
    private CheckBox integerCheckbox;

    private ArrayList<EditText> dataTypesInput;
    private ArrayList<EditText> dataCountInput;

    private DataTypeCollection dataTypeCollection;
    private Location location;

    private Resources resources;

    private MainActivity mainActivity;
    private AlertDialogHandler alertDialogHandler;

    //1 - add type, 2 - edit type, 3 - add variable, 4 - select type for state, 5 - initial marking
    private int option;

    public InputForm(Context context) {
        super(context);
        initViews(context);
        initData(location, dataTypeCollection);
    }

    public InputForm(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
        initData(location, dataTypeCollection);
    }

    public InputForm(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
        initData(location, dataTypeCollection);
    }

    public InputForm(Context context, Location location, DataTypeCollection types, int option) {
        super(context);
        this.option = option;
        initData(location, types);
        initViews(context);
        mainActivity = (MainActivity) context;
        alertDialogHandler = new AlertDialogHandler(mainActivity);
    }

    private void initData(Location location, DataTypeCollection types) {
        this.location = location;
        this.dataTypeCollection = types;
        this.dataTypesInput = new ArrayList<>();
        this.dataCountInput = new ArrayList<>();
    }

    private void initViews(Context context) {
        this.context = context;
        this.resources = context.getResources();
        inflateLayout();
        this.cancelButton = findViewById(R.id.cancel_action);
        this.addField = findViewById(R.id.add_new_field);
        this.finishAdding = findViewById(R.id.finish_adding);
        this.inputOptions = findViewById(R.id.ll_input_options);
        this.dataTypeName = findViewById(R.id.data_type_name);
        this.spinnerDataTypes = findViewById(R.id.spinner_data_types);
        this.integerCheckbox = findViewById(R.id.integer_checkbox);

        this.spinnerDataTypes.setVisibility(VISIBLE);
        dataTypeName.setVisibility(GONE);

        prepareViewsForOption();
    }

    public void initListeners() {
        integerCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for (int i = 0; i < dataTypesInput.size(); i++) {
                    EditText et = dataTypesInput.get(i);
                    et.setText("");
                    et.setInputType(b ? InputType.TYPE_CLASS_NUMBER : InputType.TYPE_CLASS_TEXT);
                }
            }
        });

        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onFinishEditing();
            }
        });

        addField.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (canAddField()) {
                    inflateConstantOrVariable(null);
                }
            }
        });

        finishAdding.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String dataName = dataTypeName.getText().toString();
                //Toast.makeText(context, String.valueOf(DataTypesValidator.isDataTypeNameUsed(dataTypeCollection, dataName)), Toast.LENGTH_SHORT).show();
                if (location == null) {
                    if (canFinishInput()) {
                        if (option == 1) {
                            DataTypeObjects dataFromInput = new DataTypeObjects();
                            for (int i = 0; i < dataTypesInput.size(); i++) {
                                if (!dataTypesInput.get(i).getText().toString().isEmpty()) {
                                    dataFromInput.add(dataTypesInput.get(i).getText().toString(), 0);
                                }
                            }
                            if (isObjectInputValid(dataFromInput)) {
                                dataTypeCollection.addNewConstants(dataName, dataFromInput);
                                mainActivity.makeValidations(null);
                            }
                        }
                        if (option == 2) {
                            String oldName = ((TextView) spinnerDataTypes.getSelectedView()).getText().toString();
                            DataTypeObjects dataFromInput = new DataTypeObjects();
                            DataTypeObjects oldVariables = dataTypeCollection.getVariablesForKey(oldName);
                            for (int i = 0; i < dataTypesInput.size(); i++) {
                                if (!dataTypesInput.get(i).getText().toString().isEmpty()) {
                                    dataFromInput.add(dataTypesInput.get(i).getText().toString(), 0);
                                }
                            }
                            dataTypeCollection.getTypes().remove(oldName);
                            dataTypeCollection.getVariables().remove(oldName);
                            if (isObjectInputValid(dataFromInput)) {
                                dataTypeCollection.addNewConstants(dataName, dataFromInput);
                                dataTypeCollection.addNewVariables(dataName, oldVariables);
                                mainActivity.makeValidations(null);
                            }
                        }
                        if (option == 3) {
                            DataTypeObjects dataFromInput = new DataTypeObjects();
                            for (int i = 0; i < dataTypesInput.size(); i++) {
                                if (!dataTypesInput.get(i).getText().toString().isEmpty()) {
                                    dataFromInput.add(dataTypesInput.get(i).getText().toString());
                                }
                            }
                            if (isObjectInputValid(dataFromInput)) {
                                dataTypeCollection.addNewVariables(dataName, dataFromInput);
                                mainActivity.makeValidations(null);
                            }
                        }
                    }
                } else {
                    if (option == 4) {
                        location.setDataTypeName(dataName);
                        location.setInitialMarking(dataTypeCollection.getConstantsForKey(dataName));
                        mainActivity.makeValidations(location);
                    } else {
                        DataTypeObjects dataFromInput = new DataTypeObjects();
                        for (int i = 0; i < dataTypesInput.size(); i++) {
                            if (!dataTypesInput.get(i).getText().toString().isEmpty()) {
                                dataFromInput.add(dataTypesInput.get(i).getText().toString(),
                                        Integer.valueOf(dataCountInput.get(i).getText().toString()));
                            }
                        }
                        location.setInitialMarking(dataFromInput);
                        mainActivity.makeValidations(location);
                    }
                    onFinishEditing();
                }
            }
        });
    }

    private void prepareViewsForOption() {
        switch (option) {
            case 1:
                addField.setText(resources.getText(R.string.add_new_value));
                integerCheckbox.setVisibility(VISIBLE);
                addField.setVisibility(VISIBLE);
                dataTypeName.setVisibility(VISIBLE);
                spinnerDataTypes.setVisibility(GONE);
                initListeners();
                break;
            case 2:
                addField.setText(resources.getText(R.string.add_new_value));
                dataTypeName.setFocusable(false);
                dataTypeName.setClickable(false);
                integerCheckbox.setVisibility(GONE);
                addField.setVisibility(VISIBLE);
                dataTypeName.setVisibility(GONE);
                spinnerDataTypes.setVisibility(VISIBLE);
                addNewDataType(option);
                break;
            case 3:
                addField.setText(resources.getText(R.string.add_variable_to_type));
                integerCheckbox.setVisibility(GONE);
                addField.setVisibility(VISIBLE);
                dataTypeName.setVisibility(GONE);
                spinnerDataTypes.setVisibility(VISIBLE);
                addNewDataType(option);
                break;
            case 4:
                integerCheckbox.setVisibility(GONE);
                addField.setVisibility(GONE);
                dataTypeName.setVisibility(GONE);
                spinnerDataTypes.setVisibility(VISIBLE);
                setTypeForState();
                break;
            case 5:
                integerCheckbox.setVisibility(GONE);
                addField.setVisibility(GONE);
                dataTypeName.setVisibility(GONE);
                spinnerDataTypes.setVisibility(GONE);
                setInitialMarkingForState();
                break;
        }
    }

    private void inflateLayout() {
        inflate(context, R.layout.variable_menu, this);
    }

    private void addNewDataType(final int isForConstant) {
        prepareSpinnerAdapter();
        spinnerDataTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                inputOptions.removeAllViews();
                initData(null, dataTypeCollection);
                dataTypeName.setText(((TextView) view).getText().toString());
                populateWithConstantOrVariable(dataTypeName.getText().toString(), isForConstant);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setTypeForState() {
        prepareSpinnerAdapter();
        spinnerDataTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                inputOptions.removeAllViews();
                initData(location, dataTypeCollection);
                dataTypeName.setText(((TextView) view).getText().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setInitialMarkingForState() {
        populateForInitialMarking(location.getDataTypeName());
    }

    private void populateWithConstantOrVariable(String dataTypeName, int isConstant) {
        ArrayList<Object> data;
        if (isConstant == 2) {
            data = dataTypeCollection.getConstantsNames(dataTypeName);
        } else {
            data = dataTypeCollection.getVariables(dataTypeName);
        }

        for (Object s : data) {
            inflateConstantOrVariable((String) s);
        }
        resetLayout();
    }

    private void inflateConstantOrVariable(String name) {
        final LinearLayout input = (LinearLayout) inflate(context, R.layout.data_type_input, null);
        final EditText editText = input.findViewById(R.id.et_expression_input);
        editText.setInputType(integerCheckbox.isChecked() ? InputType.TYPE_CLASS_NUMBER : InputType.TYPE_CLASS_TEXT);
        if (name != null && !name.isEmpty()) {
            editText.setText(name);
        }
        editText.requestFocus();
        dataTypesInput.add(editText);
        ImageView cancelIcon = input.findViewById(R.id.remove_input);
        cancelIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dataTypesInput.remove(editText);
                inputOptions.removeView(input);
                resetLayout();
            }
        });
        inputOptions.addView(input);
    }

    private void populateForInitialMarking(String name) {
        ArrayList<Integer> constantCounts = location.getInitialMarking().getObjectCount();
        ArrayList<String> constantNames = location.getInitialMarking().getObjectNames();

        for (int i = 0; i < constantNames.size(); i++) {
            inflateInitialMarking((String) constantNames.get(i), constantCounts.get(i));
        }
    }

    private void inflateInitialMarking(String name, int count) {
        final LinearLayout input = (LinearLayout) inflate(context, R.layout.initial_marking_input, null);
        TextView typeField = input.findViewById(R.id.tv_data_type);
        EditText countField = input.findViewById(R.id.et_expression_count);
        EditText nameField = input.findViewById(R.id.et_name);

        typeField.setText(location.getDataTypeName());
        countField.setText(String.valueOf(count));
        nameField.setText(name);

        dataTypesInput.add(nameField);
        dataCountInput.add(countField);

        inputOptions.addView(input);
    }

    private void prepareSpinnerAdapter() {
        ArrayList<String> data = dataTypeCollection.getDataTypeNames();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context,
                R.layout.spinner_dropdown_textview, data);
        spinnerDataTypes.setAdapter(dataAdapter);
    }

    private void onFinishEditing() {
        this.setVisibility(GONE);
        mainActivity.hideInputForm();
        View view = null;
        if (inputOptions.getChildCount() != 0) {
            view = inputOptions.getChildAt(0);
        }
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } else {
            imm.hideSoftInputFromWindow(inputOptions.getWindowToken(), 0);
        }
    }

    private boolean canAddField() {
        return (!dataTypeName.getText().toString().isEmpty() && (dataTypesInput.size() == 0
                || (dataTypesInput.get(dataTypesInput.size() - 1).getText() != null
                && !dataTypesInput.get(dataTypesInput.size() - 1).getText().toString().isEmpty())));
    }

    private boolean canFinishInput() {
        return !dataTypeName.getText().toString().isEmpty() && dataTypesInput.size() != 0
                && dataTypesInput.get(0).getText() != null && !dataTypesInput.get(0).getText().toString().isEmpty();
    }

    private boolean isObjectInputValid(DataTypeObjects constants) {
        StringBuilder message = new StringBuilder();
        ArrayList<String> usedNames = DataTypesValidator.getAlreadyUsedNames(dataTypeCollection, constants.getObjectNames());
        if (usedNames.size() != 0) {
            message.append("The following names are already used: ");
        } else {
            onFinishEditing();
            return true;
        }
        for (int i = 0; i < usedNames.size(); i++) {
            message.append(usedNames.get(i));
            if (i != usedNames.size() - 1) {
                message.append(", ");
            }
        }
        if (dataTypeCollection.getDataTypeNames().contains(dataTypeName.getText().toString()) && option == 3) {
            onFinishEditing();
            return true;
        }
        alertDialogHandler.showDialog(context, message.toString());
        return false;
    }

    private void resetLayout() {
        invalidate();
        requestLayout();
    }
}
