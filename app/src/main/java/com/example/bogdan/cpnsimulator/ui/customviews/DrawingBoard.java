package com.example.bogdan.cpnsimulator.ui.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.ui.MainActivity;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class DrawingBoard extends FrameLayout implements View.OnTouchListener {
    private ArrayList<Location> locations;
    private ArrayList<Transition> transitions;
    private CopyOnWriteArrayList<CPNLine> lines;
    private Paint paint = new Paint();
    private int color;

    private MainActivity activity;

    private int numberOfLocations;
    private int numberOfTransitions;

    public DrawingBoard(Context context) {
        super(context);
        numberOfLocations = 0;
        numberOfTransitions = 0;
        color = Color.BLACK;
        lines = new CopyOnWriteArrayList<>();
        locations = new ArrayList<>();
        transitions = new ArrayList<>();
        setWillNotDraw(false);
        activity = (MainActivity) getContext();
        this.setOnTouchListener(this);
    }

    public DrawingBoard(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        numberOfLocations = 0;
        numberOfTransitions = 0;
        color = Color.BLACK;
        lines = new CopyOnWriteArrayList<>();
        locations = new ArrayList<>();
        transitions = new ArrayList<>();
        setWillNotDraw(false);
        activity = (MainActivity) getContext();
        this.setOnTouchListener(this);
    }

    public DrawingBoard(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        numberOfLocations = 0;
        numberOfTransitions = 0;
        color = Color.BLACK;
        lines = new CopyOnWriteArrayList<>();
        locations = new ArrayList<>();
        transitions = new ArrayList<>();
        setWillNotDraw(false);
        activity = (MainActivity) getContext();
        this.setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setColor(color);

        paint.setStrokeWidth(3);

        if (lines != null && lines.size() != 0) {
            for (int i = 0; i < lines.size(); i++) {
                CPNLine line = lines.get(i);
                canvas.drawLine(line.getStart().x, line.getStart().y, line.getEnd().x, line.getEnd().y, paint);
                canvas.drawLine(line.getEnd().x, line.getEnd().y, lines.get(i).getArrowX1(), lines.get(i).getArrowY1(), paint);
                canvas.drawLine(line.getEnd().x, line.getEnd().y, lines.get(i).getArrowX2(), lines.get(i).getArrowY2(), paint);
            }
        }
    }

    public void draw(int color) {
        invalidate();
        this.color = color;
        requestLayout();
    }

    public CopyOnWriteArrayList<CPNLine> getLines() {
        return lines;
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public ArrayList<Transition> getTransitions() {
        return transitions;
    }

    public BaseEntity addBaseEntityView(boolean isLocation) {
        BaseEntity v;
        if (isLocation) {
            numberOfLocations++;
            v = new Location(activity, "L" + String.valueOf(numberOfLocations));
            this.getLocations().add((Location) v);
        } else {
            numberOfTransitions++;
            v = new Transition(activity, "T" + String.valueOf(numberOfTransitions));
            this.getTransitions().add((Transition) v);
        }

        // insert into main view
        this.addView(v, 0, new ViewGroup.LayoutParams(
                (int) activity.getResources().getDimension(R.dimen.bu_4_9),
                (int) activity.getResources().getDimension(R.dimen.bu_4_9)));
        activity.initListenersForLocationOrTransition(v);

        return v;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (activity.isDeleteModeOn()) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                PointF point = new PointF(motionEvent.getX(), motionEvent.getY());
                for (int i = 0; i < lines.size(); i++) {
                    if (this.getLines().get(i).isPointOnLine(point) || lines.get(i).pointToLineDistance(point) <= 5) {
                        onSingleClick(i);
                        return true;
                    }
                }
            }
        } else {
            activity.clearExpressionFocus(this);
            return true;
        }
        return true;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }

    public void setTransitions(ArrayList<Transition> transitions) {
        this.transitions = transitions;
    }

    public void setLines(CopyOnWriteArrayList<CPNLine> lines) {
        this.lines = lines;
    }

    public void setNumberOfLocations(int numberOfLocations) {
        this.numberOfLocations = numberOfLocations;
    }

    public void setNumberOfTransitions(int numberOfTransitions) {
        this.numberOfTransitions = numberOfTransitions;
    }

    private void onSingleClick(int i) {
        if (activity.isDeleteModeOn()) {
            draw(Color.WHITE);
            this.removeView(lines.get(i).getExpressionInput());
            lines.remove(i);
            draw(Color.BLACK);
            activity.setDeleteMode(false);
            activity.setDeleteButtonText();
        }
    }
}