package com.example.bogdan.cpnsimulator.utils;


import java.util.Arrays;


public class ExpressionParser {
    private static final String[] operators = {"!=", "==", ">=", "<=", ">", "<", "||", "&&"};

    private static boolean parseAndEvaluateExpression(String ex) {
        for (char c : ex.toCharArray()) {
            if (!Character.isSpaceChar(c))
                return parseWithStrings(ex);
        }
        return false;
    }

    public static boolean evaluate(String expression, String[] variables, String[] values) {
        if ((variables.length != values.length) && variables.length != 0) {
            return false;
        }

        for (int i = 0; i < variables.length; i++) {
            expression = expression.replace(variables[i], values[i]);
        }

        return parseAndEvaluateExpression(ExpressionUtils.getInterrogationFromExpression(expression));
    }

    private static boolean parseWithStrings(String s) {
        int[] op = determineOperatorPrecedenceAndLocation(s);
        int start = op[0];
        String left = s.substring(0, start).trim();
        String right = s.substring(op[1]).trim();
        String oper = s.substring(start, op[1]).trim();
        int logType = logicalOperatorType(oper);
        if (logType == 0) // encounters OR- recurse
            return parseWithStrings(left) || parseWithStrings(right);
        else if (logType == 1) // encounters AND- recurse
            return parseWithStrings(left) && parseWithStrings(right);
        String leftSansParen = removeParens(left);
        String rightSansParen = removeParens(right);
        if (isInt(leftSansParen) && isInt(rightSansParen))
            return evaluate(Double.parseDouble(leftSansParen), oper, Double.parseDouble(rightSansParen));
        else
            return evaluate(leftSansParen, oper, rightSansParen); // assume they are strings
    }

    private static int[] determineOperatorPrecedenceAndLocation(String s) {
        s = s.trim();
        int minParens = Integer.MAX_VALUE;
        int[] currentMin = null;
        for (int sampSize = 1; sampSize <= 2; sampSize++) {
            for (int locInStr = 0; locInStr < (s.length() + 1) - sampSize; locInStr++) {
                int endIndex = locInStr + sampSize;
                String sub;
                if ((endIndex < s.length()) && s.charAt(endIndex) == '=')
                    sub = s.substring(locInStr, ++endIndex).trim();
                else
                    sub = s.substring(locInStr, endIndex).trim();
                if (isOperator(sub)) {
                    // Idea here is to weight logical operators so that they will still be selected over other operators
                    // when no parens are present
                    int parens = (logicalOperatorType(sub) > -1) ? parens(s, locInStr) - 1 : parens(s, locInStr);
                    if (parens <= minParens) {
                        minParens = parens;
                        currentMin = new int[]{locInStr, endIndex, parens};
                    }
                }
            }
        }
        return currentMin;
    }

    private static int logicalOperatorType(String op) {
        switch (op.trim()) {
            case "||":
                return 0;
            case "&&":
                return 1;
            default:
                return -1;
        }
    }

    private static int parens(String s, int loc) {
        int parens = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(' && i < loc)
                parens++;
            if (s.charAt(i) == ')' && i >= loc)
                parens++;
        }
        return parens;
    }

    private static String removeParens(String s) {
        s = s.trim();
        StringBuilder keep = new StringBuilder();
        for (char c : s.toCharArray()) {
            if (!(c == '(') && !(c == ')'))
                keep.append(c);
        }
        return keep.toString().trim();
    }

    private static boolean isOperator(String op) {
        op = op.trim();
        return Arrays.asList(operators).contains(op);
    }

    private static boolean isInt(String s) {
        for (char c : s.toCharArray())
            if (!Character.isDigit(c) && c != '.')
                return false;
        return true;
    }

    private static boolean evaluate(double left, String op, double right) {
        switch (op) {
            case "==":
                return left == right;
            case ">":
                return left > right;
            case "<":
                return left < right;
            case "<=":
                return left <= right;
            case ">=":
                return left >= right;
            case "!=":
                return left != right;
            default:
                System.err.println("ERROR: Operator type not recognized.");
                return false;
        }
    }

    private static boolean evaluate(String left, String op, String right) {
        switch (op) {
            case "==":
                return left.equals(right);
            case "!=":
                return !left.equals(right);
            default:
                System.err.println("ERROR: Operator type not recognized.");
                return false;
        }
    }
}