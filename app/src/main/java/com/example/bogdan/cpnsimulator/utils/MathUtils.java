package com.example.bogdan.cpnsimulator.utils;

/**
 * Created by bpristavu on 1/17/2018.
 */

public class MathUtils {
  public static int getMax(int a, int b) {
    if (a >= b) {
      return a;
    } else {
      return b;
    }
  }

  public static int getMin(int a, int b){
    if (a <= b) {
      return a;
    } else {
      return b;
    }
  }
}
