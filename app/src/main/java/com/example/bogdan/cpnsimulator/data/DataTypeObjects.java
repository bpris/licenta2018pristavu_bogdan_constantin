package com.example.bogdan.cpnsimulator.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by bpristavu on 1/31/2018.
 */

public class DataTypeObjects {
    private Map<String, Integer> constants;

    public DataTypeObjects() {
        this.constants = new TreeMap<>();
    }

    public DataTypeObjects(TreeMap<String, Integer> constants) {
        this.constants = constants;
    }

    public ArrayList<String> getObjectNames() {
        ArrayList<String> data = new ArrayList<>();
        data.addAll(constants.keySet());
        //subtract(this);
        return data;
    }

    public ArrayList<Integer> getObjectCount() {
        ArrayList<Integer> data = new ArrayList<>();
        for (Object s : constants.keySet()) {
            data.add(constants.get(s));
        }
        return data;
    }

    public void add(String objectName) {
        constants.put(objectName, 0);
    }

    public void add(String objectName, int count) {
        constants.put(objectName, count);
    }

    public DataTypeObjects add(DataTypeObjects c2) {
        for (String key : constants.keySet()) {
            if (c2.getObjects().get(key) != null) {
                constants.put(key, constants.get(key) + c2.getObjects().get(key));
            }
        }
        return this;
    }

    public DataTypeObjects subtract(DataTypeObjects c2) {
        for (String key : constants.keySet()) {
            if (c2.getObjects().get(key) != null) {
                if (constants.get(key) - c2.getObjects().get(key) >= 0) {
                    constants.put(key, constants.get(key) - c2.getObjects().get(key));
                } else {
                    return null;
                }
            }
        }
        return this;
    }

    public Map<String, Integer> getObjects() {
        return constants;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!DataTypeObjects.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        DataTypeObjects aux = (DataTypeObjects) obj;
        return this.constants.keySet().equals(aux.getObjects().keySet());
    }
}
