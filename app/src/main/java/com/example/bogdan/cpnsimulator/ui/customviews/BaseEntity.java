package com.example.bogdan.cpnsimulator.ui.customviews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.json.JSONObject;

/**
 * Created by Bogdan on 15.01.2018.
 */

public abstract class BaseEntity extends FrameLayout {
    private TextView viewName;
    private EditText editViewName;
    private String name;
    private InputMethodManager imm;

    public static final String JSON_NAME_KEY = "view_name";
    public static final String JSON_X_COORDINATE = "x_coordinate";
    public static final String JSON_Y_COORDINATE = "y_coordinate";

    private boolean isEditing;

    public BaseEntity(@NonNull Context context) {
        super(context);
    }

    public BaseEntity(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseEntity(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public BaseEntity(Context context, String name) {
        super(context);
        init(name);
    }

    protected abstract void inflateLayout();

    public abstract TextView getViewName();

    public abstract EditText getEditViewName();

    public void forceCloseAll() {
        onNameChanged();
    }

    public void changeName() {
        isEditing = true;
        viewName.setVisibility(GONE);
        editViewName.setText(name);
        editViewName.setVisibility(VISIBLE);
        editViewName.requestFocus();
        editViewName.setSelection(editViewName.getText().length());
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void onNameChanged() {
        isEditing = false;
        viewName.setVisibility(VISIBLE);
        editViewName.setVisibility(GONE);
        name = editViewName.getText().toString();
        viewName.setText(editViewName.getText().toString());
        imm.hideSoftInputFromWindow(editViewName.getWindowToken(), 0);
    }

    private void init(String name) {
        inflateLayout();
        isEditing = false;
        this.viewName = getViewName();
        this.editViewName = getEditViewName();
        this.name = name;
        if (name.length() >= 3) {
            this.viewName.setText(name.substring(0, 3));
        } else {
            this.viewName.setText(name);
        }
        this.editViewName.setText(name);
        imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public boolean isEditing() {
        return isEditing;
    }

    public void setEditing(boolean editing) {
        isEditing = editing;
    }

    public String getName() {
        return name;
    }

    public abstract JSONObject toJSON();

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!BaseEntity.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        BaseEntity aux = (BaseEntity) obj;
        return this.getName().equals(aux.getName());
    }
}
