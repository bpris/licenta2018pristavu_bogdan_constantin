package com.example.bogdan.cpnsimulator.ui.customviews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.DataTypeCollection;
import com.example.bogdan.cpnsimulator.data.DataTypeObjects;
import com.example.bogdan.cpnsimulator.ui.MainActivity;

import java.util.ArrayList;

public class InitialMarkingInputForm extends LinearLayout {
    private Button confirmButton;
    private LinearLayout inputZone;

    private Location location;
    private DataTypeCollection data;
    private Context context;

    private ArrayList<EditText> dataTypesInput;
    private ArrayList<EditText> dataCountInput;

    public InitialMarkingInputForm(Context context) {
        super(context);
    }

    public InitialMarkingInputForm(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public InitialMarkingInputForm(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public InitialMarkingInputForm(Context context, Location location, DataTypeCollection data) {
        super(context);
        this.context = context;
        this.location = location;
        this.data = data;
        initViews();
        initData();
    }

    private void initViews() {
        inflateLayout();
        confirmButton = findViewById(R.id.confirm_button);
        inputZone = findViewById(R.id.ll_input_zone);
        initListeners();
    }

    private void initData() {
        dataTypesInput = new ArrayList<>();
        dataCountInput = new ArrayList<>();
        populateWithData();
    }

    private void inflateLayout() {
        inflate(context, R.layout.initial_marking_layout, this);
    }

    private void initListeners() {
        confirmButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInitialMarking();
                InitialMarkingInputForm.this.setVisibility(GONE);
                View view = null;
                if (inputZone.getChildCount() != 0) {
                    view = inputZone.getChildAt(0);
                }
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (view != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else {
                    imm.hideSoftInputFromWindow(inputZone.getWindowToken(), 0);
                }
                ((MainActivity) context).hideInitialMarkingForm();
            }
        });
    }

    private void populateWithData() {
        ArrayList<Integer> constantCounts = location.getInitialMarking().getObjectCount();
        ArrayList<String> constantNames = location.getInitialMarking().getObjectNames();

        for (int i = 0; i < constantNames.size(); i++) {
            inflateInitialMarking((String) constantNames.get(i), constantCounts.get(i));
        }
    }

    private void inflateInitialMarking(String name, int count) {
        final LinearLayout input = (LinearLayout) inflate(context, R.layout.initial_marking_input, null);
        TextView typeField = input.findViewById(R.id.tv_data_type);
        EditText countField = input.findViewById(R.id.et_expression_count);
        EditText nameField = input.findViewById(R.id.et_name);

        typeField.setText(location.getDataTypeName());
        countField.setText(String.valueOf(count));
        nameField.setText(name);

        dataTypesInput.add(nameField);
        dataCountInput.add(countField);

        inputZone.addView(input);
    }

    private void saveInitialMarking() {
        DataTypeObjects dataFromInput = new DataTypeObjects();
        for (int i = 0; i < dataTypesInput.size(); i++) {
            if (!dataTypesInput.get(i).getText().toString().isEmpty()) {
                dataFromInput.add(dataTypesInput.get(i).getText().toString(),
                        Integer.valueOf(dataCountInput.get(i).getText().toString()));
            }
        }
        location.setInitialMarking(dataFromInput);
    }
}
