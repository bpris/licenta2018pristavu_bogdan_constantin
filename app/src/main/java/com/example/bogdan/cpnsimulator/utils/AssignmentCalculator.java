package com.example.bogdan.cpnsimulator.utils;

import com.example.bogdan.cpnsimulator.ui.customviews.CPNLine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by bpristavu on 5/2/2018.
 */

public class AssignmentCalculator {

    public static Set<Map<String, String>> calculate(Map<String, Integer> values, Map<String, Integer> variables, Set<Map<String, String>> assignments) {
        Iterator<Map<String, String>> it = assignments.iterator();
        if (variables != null && !variables.isEmpty()) {
            while (it.hasNext()) {
                Map<String, String> assignment = it.next();
                if (!isAssignmentValid(values, variables, assignment)) {
                    it.remove();
                }
            }
        } else {
            while (it.hasNext()) {
                Map<String, String> assignment = it.next();

            }
        }

        return assignments;
    }

    private static boolean isAssignmentValid(Map<String, Integer> values, Map<String, Integer> variables, Map<String, String> assignment) {
        Map<String, Integer> tempValues = new LinkedHashMap<>();
        tempValues.putAll(values);
        Iterator assignmentIt = assignment.entrySet().iterator();
        while (assignmentIt.hasNext()) {
            Map.Entry pair = (Map.Entry) assignmentIt.next();
            if (variables.containsKey((String) pair.getKey()) && tempValues.containsKey((String) pair.getValue())) {
                if (variables.get((String) pair.getKey()) > tempValues.get((String) pair.getValue())) {
                    return false;
                } else {
                    int difference = tempValues.get(pair.getValue()).intValue() - variables.get(pair.getKey()).intValue();
                    tempValues.put((String) pair.getValue(), difference);
                }
            }
        }
        return true;
    }

    public static List<Set<Map<String, String>>> cartesianProduct
            (Set<String> values, Set<String> variables) {
        List<Set<Map<String, String>>> result = new ArrayList<>();

        for (String variable : variables) {
            Set<Map<String, String>> variablesAssignments = new LinkedHashSet<>();
            for (String value : values) {
                Map<String, String> partialResult = new LinkedHashMap<>();
                partialResult.put(variable, value);
                variablesAssignments.add(partialResult);
            }
            result.add(variablesAssignments);
        }

        return result;
    }

    @SafeVarargs
    public static Set<Map<String, String>> cartesianProduct(int index, Set<Map<String, String>>... sets) {
        Set<Map<String, String>> ret = new LinkedHashSet<>();
        if (index == sets.length) {
            ret.add(new TreeMap<String, String>());
        } else {
            for (Map<String, String> obj : sets[index]) {
                for (Map<String, String> map : cartesianProduct(index + 1, sets)) {
                    map.putAll(obj);
                    ret.add(map);
                }
            }
        }
        return ret;
    }

    public static Set<Map<String, String>> getCommonAssignments(List<CPNLine> inputLines, String guardExpression) {
        Set<Map<String, String>> result;
        if (inputLines != null && inputLines.size() > 0) {
            result = inputLines.get(0).getCPNExpression().getAssignmentsForLine();
        } else {
            return null;
        }

        if (inputLines.size() > 1) {
            for (int i = 1; i < inputLines.size(); i++) {
                result.retainAll(inputLines.get(i).getCPNExpression().getAssignmentsForLine());
            }
        }

        Set<Map<String, String>> possibleAssignments;
        Set<Map<String, String>> notPossibleAssignments;
        if (guardExpression != null && !result.isEmpty()) {
            possibleAssignments = GuardExpressionUtils.getPossibleAssignment(guardExpression);
            notPossibleAssignments = GuardExpressionUtils.getNotPossibleAssignment(guardExpression);
            Iterator<Map<String, String>> assignmentsIt = result.iterator();
            Iterator<Map<String, String>> possibleAssignmentsIt = null;
            Iterator<Map<String, String>> notPossibleAssignmentsIt = null;
            while (assignmentsIt.hasNext()) {
                if (possibleAssignments != null && !possibleAssignments.isEmpty()) {
                    possibleAssignmentsIt = possibleAssignments.iterator();
                }
                if (notPossibleAssignments != null && !notPossibleAssignments.isEmpty()) {
                    notPossibleAssignmentsIt = notPossibleAssignments.iterator();
                }
                Map<String, String> assignment = assignmentsIt.next();

                if (possibleAssignmentsIt != null) {
                    while (possibleAssignmentsIt.hasNext()) {
                        Map<String, String> possibleAssignment = possibleAssignmentsIt.next();
                        if (!assignment.entrySet().containsAll(possibleAssignment.entrySet())) {
                            assignmentsIt.remove();
                            break;
                        }
                    }
                }

                if (notPossibleAssignmentsIt != null) {
                    while (notPossibleAssignmentsIt.hasNext()) {
                        Map<String, String> notPossibleAssignment = notPossibleAssignmentsIt.next();
                        if (assignment.entrySet().containsAll(notPossibleAssignment.entrySet())) {
                            assignmentsIt.remove();
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }
}
