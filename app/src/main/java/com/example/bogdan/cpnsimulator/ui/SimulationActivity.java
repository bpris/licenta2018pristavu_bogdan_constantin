package com.example.bogdan.cpnsimulator.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.text.InputType;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.CPNExpression;
import com.example.bogdan.cpnsimulator.data.CacheManager;
import com.example.bogdan.cpnsimulator.data.DataTypeCollection;
import com.example.bogdan.cpnsimulator.data.DataTypeObjects;
import com.example.bogdan.cpnsimulator.ui.customviews.BaseEntity;
import com.example.bogdan.cpnsimulator.ui.customviews.CPNLine;
import com.example.bogdan.cpnsimulator.ui.customviews.CustomScrollView;
import com.example.bogdan.cpnsimulator.ui.customviews.Location;
import com.example.bogdan.cpnsimulator.ui.customviews.NetStateForm;
import com.example.bogdan.cpnsimulator.ui.customviews.SimulationBoard;
import com.example.bogdan.cpnsimulator.ui.customviews.Transition;
import com.example.bogdan.cpnsimulator.utils.Constants;
import com.example.bogdan.cpnsimulator.utils.ExpressionParser;
import com.example.bogdan.cpnsimulator.utils.ExpressionUtils;
import com.example.bogdan.cpnsimulator.utils.AssignmentCalculator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class SimulationActivity extends Activity {

    private SimulationBoard simulationBoard;
    private FrameLayout screenContainer;
    private LinearLayout toolbar;
    private ListView arrowSelection;
    private DataTypeCollection dataTypeCollection;

    private CustomScrollView verticalScrollView;

    private Button menuButton;

    private Button multipleStepsButton;
    private Button oneStepButton;
    private Button oneRewindButton;
    private Button revertButton;

    private JSONObject initialState;

    private NetStateForm netStateForm;

    private Resources resources;

    private ArrayList<JSONObject> netStates;
    private boolean isStepInitialized;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation);
        String fileContent = getIntent().getStringExtra(Constants.INTENT_KEY);
        initViews();
        initData();
        try {
            initialState = new JSONObject(fileContent);
            loadNetwork(initialState);
            netStates.add(initialState);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void initViews() {
        verticalScrollView = findViewById(R.id.vertical_scrollable_surface);
        screenContainer = findViewById(R.id.screen_container);
        simulationBoard = findViewById(R.id.simulation_board);
        toolbar = findViewById(R.id.toolbar);
        arrowSelection = findViewById(R.id.lv_arrow_selection);
        menuButton = findViewById(R.id.open_menu);


        multipleStepsButton = findViewById(R.id.btn_multiple_steps);
        oneStepButton = findViewById(R.id.btn_one_step);
        oneRewindButton = findViewById(R.id.btn_one_rewind);
        revertButton = findViewById(R.id.btn_reset_layout);

        initListeners();
    }

    private void initListeners() {
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNetStateForm();
            }
        });

        multipleStepsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNumericInputDialog();
            }
        });

        oneStepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SimulationActivity.this, "Please select a transition", Toast.LENGTH_LONG).show();
                isStepInitialized = true;
            }
        });

        oneRewindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (netStates.size() > 1) {
                    netStates.remove(netStates.size() - 1);
                    loadNetwork(netStates.get(netStates.size() - 1));
                }
            }
        });

        revertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadNetwork(initialState);
                netStates.clear();
                netStates.add(initialState);
            }
        });
    }

    private void showNetStateForm() {
        verticalScrollView.setVisibility(View.GONE);
        netStateForm = new NetStateForm(this, simulationBoard.getLocations());
        netStateForm.setVisibility(View.VISIBLE);
        screenContainer.addView(netStateForm, screenContainer.getChildCount());
        toolbar.setVisibility(View.INVISIBLE);
    }

    public void hideNetStateForm() {
        verticalScrollView.setVisibility(View.VISIBLE);
        screenContainer.removeView(netStateForm);
        netStateForm = null;
        toolbar.setVisibility(View.VISIBLE);
    }

    private void initData() {
        resources = getResources();
        netStates = new ArrayList<>();
        isStepInitialized = false;
    }

    public void initListenersForLocationOrTransition(View view) {
        view.setOnTouchListener(new View.OnTouchListener() {
            private int xDelta;
            private int yDelta;

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                moveElementsIfConnected(view);

                final int xPos = (int) event.getRawX();
                final int yPos = (int) event.getRawY();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        if (isStepInitialized && view instanceof Transition) {
                            showDialogForAssignments((Transition) view);
                        }
                        FrameLayout.LayoutParams lParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                        xDelta = xPos - lParams.leftMargin;
                        yDelta = yPos - lParams.topMargin;
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                        layoutParams.leftMargin = xPos - xDelta;
                        layoutParams.topMargin = yPos - yDelta;
                        if (layoutParams.leftMargin >= simulationBoard.getWidth() - view.getWidth()) {
                            layoutParams.leftMargin = simulationBoard.getWidth() - view.getWidth();
                        }
                        if (layoutParams.leftMargin < 0) {
                            layoutParams.leftMargin = 0;
                        }
                        if (layoutParams.topMargin >= simulationBoard.getHeight() - view.getHeight()) {
                            layoutParams.topMargin = simulationBoard.getHeight() - view.getHeight();
                        }
                        if (layoutParams.topMargin < 0) {
                            layoutParams.topMargin = 0;
                        }
                        view.setLayoutParams(layoutParams);
                        break;
                }
                return true;
            }
        });
    }

    public void showNumericInputDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please enter the number of steps to be executed");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                executeMultipleCalculations(Integer.valueOf(input.getText().toString()));
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    public boolean clearExpressionFocus(View view) {
        if (simulationBoard.getFocusedChild() != null && simulationBoard.getFocusedChild() instanceof EditText) {
            simulationBoard.getFocusedChild().clearFocus();
            simulationBoard.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        return false;
    }

    public void drawNewLine(BaseEntity start, BaseEntity end) {
        CPNLine cpnLine = new CPNLine(start, end, toolbar.getHeight());
        String lastValue = null;

        if (simulationBoard.getLines().size() != 0 && simulationBoard.getLines().contains(cpnLine)) {
            CPNLine toBeReplacedLine = simulationBoard.getLines().get(simulationBoard.getLines().indexOf(cpnLine));
            if (toBeReplacedLine.getCPNExpression() != null &&
                    toBeReplacedLine.getCPNExpression().getExpression() != null
                    && !toBeReplacedLine.getCPNExpression().getExpression().isEmpty()) {
                lastValue = toBeReplacedLine.getExpressionInput().getText().toString();
            }
            simulationBoard.removeView(toBeReplacedLine.getExpressionInput());
            simulationBoard.getLines().remove(toBeReplacedLine);
        }
        simulationBoard.getLines().add(cpnLine);

        simulationBoard.draw(Color.BLACK);
        EditText expression = (EditText) getLayoutInflater().inflate(R.layout.arrow_expression_input, simulationBoard, false);
        DataTypeObjects types;
        DataTypeObjects variables;
        if (start instanceof Location) {
            types = ((Location) start).getInitialMarking();
            variables = dataTypeCollection.getVariablesForKey(((Location) start).getDataTypeName());
        } else {
            types = ((Location) end).getInitialMarking();
            variables = dataTypeCollection.getVariablesForKey(((Location) end).getDataTypeName());
        }
        cpnLine.setExpressionInput(expression, lastValue, types, variables);
        simulationBoard.addView(cpnLine.getExpressionInput(), 0);
        redrawViewOnArrow(cpnLine.getExpressionInput(), cpnLine);
    }

    private void moveElementsIfConnected(View view) {
        if (simulationBoard.getLines().size() != 0) {
            for (int i = 0; i < simulationBoard.getLines().size(); i++) {
                if (view.equals(simulationBoard.getLines().get(i).getStartView())) {
                    drawNewLine((BaseEntity) view, (BaseEntity) simulationBoard.getLines().get(i).getEndView());
                    redrawViewOnArrow(arrowSelection, simulationBoard.getLines().get(i));
                }
                if (view.equals(simulationBoard.getLines().get(i).getEndView())) {
                    drawNewLine((BaseEntity) simulationBoard.getLines().get(i).getStartView(), (BaseEntity) view);
                    redrawViewOnArrow(arrowSelection, simulationBoard.getLines().get(i));
                }
            }
        }
    }

    private void redrawViewOnArrow(View view, CPNLine line) {
        view.setX(line.getExpressionLocation().x);
        view.setY(line.getExpressionLocation().y);
        float angle = (float) line.getAngle();
        if (angle >= 90 && angle <= 180) {
            angle -= 180;
        } else {
            if (angle <= -90 && angle >= -180) {
                angle += 180;
            }
        }
        //view.setRotation(angle);
    }

    private void clearData() {
        simulationBoard.removeAllViews();
        simulationBoard.setLines(new CopyOnWriteArrayList<CPNLine>());
        simulationBoard.setLocations(new ArrayList<Location>());
        simulationBoard.setTransitions(new ArrayList<Transition>());
        this.dataTypeCollection = new DataTypeCollection();
    }

    public void loadNetwork(final JSONObject fileContent) {
        clearData();
        simulationBoard.addView(arrowSelection);
        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ArrayList<Location> locations = CacheManager.getLocationFromFile(fileContent);
                if (locations != null) {
                    simulationBoard.setNumberOfLocations(locations.size());
                    for (int i = 0; i < locations.size(); i++) {
                        Location location = new Location(SimulationActivity.this, locations.get(i).getName());
                        simulationBoard.getLocations().add(location);
                        simulationBoard.addView(location, 0, new ViewGroup.LayoutParams(
                                (int) resources.getDimension(R.dimen.bu_5),
                                (int) resources.getDimension(R.dimen.bu_5)));
                        initListenersForLocationOrTransition(simulationBoard.getLocations().get(i));
                        if (locations.get(i).getDataTypeName() != null) {
                            simulationBoard.getLocations().get(i).setDataTypeName(locations.get(i).getDataTypeName());
                        }
                        if (locations.get(i).getInitialMarking() != null) {
                            simulationBoard.getLocations().get(i).setInitialMarking(locations.get(i).getInitialMarking());
                        }
                        MotionEvent motionEvent = MotionEvent.obtain(
                                0,
                                0,
                                MotionEvent.ACTION_MOVE,
                                locations.get(i).getX(),
                                locations.get(i).getY(),
                                0
                        );
                        simulationBoard.getLocations().get(i).dispatchTouchEvent(motionEvent);
                    }
                }
            }
        }, 200);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ArrayList<Transition> transitions = CacheManager.getTransitionsFromFile(fileContent);
                if (transitions != null) {
                    simulationBoard.setNumberOfTransitions(transitions.size());
                    for (int i = 0; i < transitions.size(); i++) {
                        Transition transition = new Transition(SimulationActivity.this, transitions.get(i).getName());
                        simulationBoard.getTransitions().add(transition);
                        simulationBoard.addView(transition, 0, new ViewGroup.LayoutParams(
                                (int) resources.getDimension(R.dimen.bu_5),
                                (int) resources.getDimension(R.dimen.bu_5)));
                        initListenersForLocationOrTransition(simulationBoard.getTransitions().get(i));
                        if (transitions.get(i).getGuardExpression() != null) {
                            simulationBoard.getTransitions().get(i).onGuardExpressionSet(transitions.get(i).getGuardExpression());
                        }
                        MotionEvent motionEvent = MotionEvent.obtain(
                                0,
                                0,
                                MotionEvent.ACTION_MOVE,
                                transitions.get(i).getX(),
                                transitions.get(i).getY(),
                                0
                        );
                        simulationBoard.getTransitions().get(i).dispatchTouchEvent(motionEvent);
                    }
                }
            }
        }, 400);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ArrayList<CPNLine> cpnLines = CacheManager.getCPNLinesFromFile(fileContent);
                if (cpnLines != null) {
                    for (int i = 0; i < cpnLines.size(); i++) {
                        CPNLine cpnLine = cpnLines.get(i);
                        if (cpnLine.getStartView() instanceof Location) {
                            simulationBoard.getLocations().contains(cpnLine.getStartView());
                            simulationBoard.getTransitions().contains(cpnLine.getEndView());
                            drawNewLine(
                                    simulationBoard.getLocations().get(simulationBoard.getLocations().indexOf(cpnLine.getStartView())),
                                    simulationBoard.getTransitions().get(simulationBoard.getTransitions().indexOf(cpnLine.getEndView())));
                        } else {
                            simulationBoard.getTransitions().contains(cpnLine.getStartView());
                            simulationBoard.getLocations().contains(cpnLine.getEndView());
                            drawNewLine(
                                    simulationBoard.getTransitions().get(simulationBoard.getTransitions().indexOf(cpnLine.getStartView())),
                                    simulationBoard.getLocations().get(simulationBoard.getLocations().indexOf(cpnLine.getEndView())));
                        }
                        if (cpnLines.get(i).getCPNExpression() != null) {
                            simulationBoard.getLines().get(i).setExpressionInput(
                                    simulationBoard.getLines().get(i).getExpressionInput(),
                                    cpnLine.getCPNExpression().getExpression(),
                                    cpnLine.getCPNExpression().getValues(),
                                    cpnLine.getCPNExpression().getVariables());
                        }
                    }
                }
            }
        }, 600);
        simulationBoard.draw(Color.BLACK);
        this.dataTypeCollection = CacheManager.getDataCollectionFromFile(fileContent);
    }

    public void executeMultipleCalculations(int numberOfSteps) {
        Random random = new Random();
        List<Map<String, String>> executedAssignments = new ArrayList<>();
        List<Transition> executedTransitions = new ArrayList<>();
        for (int i = 0; i < numberOfSteps; i++) {
            ArrayList<Transition> transitions = getTransitionsWithPossibleAssignments();
            if (transitions.size() == 0) {
                Toast.makeText(this, "There are no more possible operations", Toast.LENGTH_LONG).show();
                showExecuteOperationsDialog(executedAssignments, executedTransitions);
                return;
            }
            Transition transition = transitions.get(random.nextInt(transitions.size()));
            ArrayList<Map<String, String>> assignments =
                    new ArrayList<>(getPossibleAssignmentsForTransition(transition));
            Map<String, String> assignment = assignments.get(random.nextInt(assignments.size()));
            executedAssignments.add(assignment);
            executedTransitions.add(transition);
            executeOperationForAssignment(transition, assignment);
        }
        showExecuteOperationsDialog(executedAssignments, executedTransitions);
    }

    private void showExecuteOperationsDialog(List<Map<String, String>> operations, List<Transition> executedTransitions) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("List of executed operations");
        ArrayList<String> rows = new ArrayList<>();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        for (int i = 0; i < operations.size(); i++) {
            StringBuilder row = new StringBuilder();
            row.append(i + 1);
            row.append(".");
            row.append("Transition ");
            row.append(executedTransitions.get(i).getName());
            row.append(": ");
            for (String key : operations.get(i).keySet()) {
                row.append(key);
                row.append("=");
                row.append(operations.get(i).get(key));
                row.append(", ");
            }
            rows.add(row.toString());
        }
        arrayAdapter.addAll(rows);
        builder.setAdapter(arrayAdapter, null);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    public ArrayList<Transition> getTransitionsWithPossibleAssignments() {
        ArrayList<Transition> transitions = new ArrayList<>();
        for (Transition transition : simulationBoard.getTransitions()) {
            Set<Map<String, String>> result = getPossibleAssignmentsForTransition(transition);
            if (result != null && result.size() != 0) {
                transitions.add(transition);
            }
        }
        return transitions;
    }

    public Set<Map<String, String>> getPossibleAssignmentsForTransition(Transition transition) {
        List<CPNLine> inputLines = new ArrayList<>();
        String guardExpression = null;
        for (CPNLine line : simulationBoard.getLines()) {
            if (line.getEndView().equals(transition)) {
                inputLines.add(line);
                guardExpression = ((Transition) line.getEndView()).getGuardExpression();
            }
        }
        return AssignmentCalculator.getCommonAssignments(inputLines, guardExpression);
    }


    private void showDialogForAssignments(Transition transition) {
        Set<Map<String, String>> possibleAssignments = getPossibleAssignmentsForTransition(transition);

        showListDialog(transition, possibleAssignments);
    }

    private void showListDialog(final Transition transition, final Set<Map<String, String>> assignments) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Possible assignments are :");

        ArrayList<String> rows = new ArrayList<>();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        for (Map<String, String> assignment : assignments) {
            StringBuilder row = new StringBuilder();
            for (String key : assignment.keySet()) {
                row.append(key);
                row.append("=");
                row.append(assignment.get(key));
                row.append(", ");
            }
            row.setLength(row.length() - 2);
            rows.add(row.toString());
        }
        arrayAdapter.addAll(rows);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                String strName = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(SimulationActivity.this);
                builderInner.setMessage(strName);
                builderInner.setTitle("Your selected assignment is");
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        isStepInitialized = false;
                        List<Map<String, String>> aux = new ArrayList<>(assignments);
                        executeOperationForAssignment(transition, aux.get(which));
                        dialog.dismiss();
                    }
                });

                builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog.dismiss();
                        showListDialog(transition, assignments);
                    }
                });
                builderInner.show();
            }
        });

        builder.show();
    }

    private void executeOperationForAssignment(Transition transition, Map<String, String> assignment) {
        List<CPNLine> inputLines = new ArrayList<>();
        List<CPNLine> outputLines = new ArrayList<>();
        for (CPNLine line : simulationBoard.getLines()) {
            if (line.getEndView().equals(transition)) {
                inputLines.add(line);
            }
        }

        for (CPNLine line : simulationBoard.getLines()) {
            if (line.getStartView().equals(transition)) {
                outputLines.add(line);
            }
        }

        for (CPNLine line : inputLines) {
            String expression = line.getCPNExpression().getExpression();
            DataTypeObjects values = line.getCPNExpression().getValues();
            DataTypeObjects variables = line.getCPNExpression().getVariables();
            String statement;
            if (ExpressionUtils.hasInterrogation(expression)) {
                boolean expressionResult = ExpressionParser.evaluate(
                        expression,
                        variables.getObjects().keySet().toArray(new String[variables.getObjects().size()]),
                        values.getObjects().keySet().toArray(new String[values.getObjects().size()]));
                if (expressionResult) {
                    statement = ExpressionUtils.getThenStatementFromExpression(expression);
                } else {
                    statement = ExpressionUtils.getElseStatementFromExpression(expression);
                }
            } else {
                statement = expression;
            }
            for (String s : assignment.keySet()) {
                statement = statement.replace(s, assignment.get(s));
            }
            DataTypeObjects toBeExtracted = ExpressionUtils.getSpecificObjectsFromStatement(statement, values);
            ((Location) line.getStartView()).getInitialMarking().subtract(toBeExtracted);
            line.getCPNExpression().setValues(((Location) line.getStartView()).getInitialMarking());
        }

        for (CPNLine line : outputLines) {
            String expression = line.getCPNExpression().getExpression();
            DataTypeObjects values = line.getCPNExpression().getValues();
            DataTypeObjects variables = line.getCPNExpression().getVariables();
            String statement;
            ArrayList<String> tempValues = new ArrayList<>();
            ArrayList<String> tempVariables = new ArrayList<>();
            if (assignment != null) {
                for (String s : assignment.keySet()) {
                    tempVariables.add(s);
                    tempValues.add(assignment.get(s));
                }
            }
            if (ExpressionUtils.hasInterrogation(expression)) {
                boolean expressionResult = ExpressionParser.evaluate(
                        expression,
                        tempVariables.toArray(new String[tempVariables.size()]),
                        tempValues.toArray(new String[tempValues.size()]));
                if (expressionResult) {
                    statement = ExpressionUtils.getThenStatementFromExpression(expression);
                } else {
                    statement = ExpressionUtils.getElseStatementFromExpression(expression);
                }
            } else {
                statement = expression;
            }
            for (String s : assignment.keySet()) {
                statement = statement.replace(s, assignment.get(s));
            }
            DataTypeObjects toBeAdded = ExpressionUtils.getSpecificObjectsFromStatement(statement, values);
            ((Location) line.getEndView()).getInitialMarking().add(toBeAdded);

            for (CPNLine aux : simulationBoard.getLines()) {
                if(line.getEndView().equals(aux.getStartView())){
                    aux.getCPNExpression().setValues(((Location) line.getEndView()).getInitialMarking());
                }
            }
            line.getCPNExpression().setValues(((Location) line.getEndView()).getInitialMarking());
        }

        netStates.add(CacheManager.createFileContent(
                simulationBoard.getLocations(),
                simulationBoard.getTransitions(),
                simulationBoard.getLines(),
                dataTypeCollection));
    }

    @Override
    public void onBackPressed() {
        if (netStateForm != null) {
            hideNetStateForm();
        } else {
            super.onBackPressed();
        }
    }
}
