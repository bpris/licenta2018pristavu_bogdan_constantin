package com.example.bogdan.cpnsimulator.utils;

import com.example.bogdan.cpnsimulator.data.DataTypeCollection;

import java.util.ArrayList;

/**
 * Created by Bogdan on 31.01.2018.
 */

public class DataTypesValidator {

    public static boolean isDataTypeNameUsed(DataTypeCollection dataTypeCollection, String name) {
        return dataTypeCollection.getDataTypeNames().contains(name);
    }

    public static boolean areConstantOrVariableNamesUsed(DataTypeCollection dataTypeCollection, ArrayList<String> names) {
        for (String input : names) {
            for (String dataTypeNames : dataTypeCollection.getDataTypeNames()) {
                if (dataTypeCollection.getConstantsNames(dataTypeNames).contains(input)) {
                    return true;
                }

                if (dataTypeCollection.getVariables(dataTypeNames).contains(input)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static ArrayList<String> getAlreadyUsedNames(DataTypeCollection dataTypeCollection, ArrayList<String> names) {
        ArrayList<String> usedNames = new ArrayList<>();
        for (String  input : names) {
            for (String dataTypeNames : dataTypeCollection.getDataTypeNames()) {
                if (dataTypeCollection.getConstantsNames(dataTypeNames).contains(input)) {
                    usedNames.add(input);
                }

                if (dataTypeCollection.getVariables(dataTypeNames).contains(input)) {
                    usedNames.add(input);
                }
            }
        }
        return usedNames;
    }
}
