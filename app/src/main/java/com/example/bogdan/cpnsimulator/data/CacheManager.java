package com.example.bogdan.cpnsimulator.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.bogdan.cpnsimulator.ui.AlertDialogHandler;
import com.example.bogdan.cpnsimulator.ui.MainActivity;
import com.example.bogdan.cpnsimulator.ui.customviews.CPNLine;
import com.example.bogdan.cpnsimulator.ui.customviews.Location;
import com.example.bogdan.cpnsimulator.ui.customviews.Transition;
import com.example.bogdan.cpnsimulator.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by bpristavu on 5/22/2018.
 */

public class CacheManager {
    private static SharedPreferences preferences;
    private static Context context;

    public CacheManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public static void saveFile(String fileName,
                                ArrayList<Location> locations,
                                ArrayList<Transition> transitions,
                                CopyOnWriteArrayList<CPNLine> lines,
                                DataTypeCollection data) {
        if (preferences.getString(fileName, null) == null) {
            preferences.edit().putString(fileName, createFileContent(locations, transitions, lines, data).toString()).apply();
            ((MainActivity) context).setLastFileName(fileName);
        } else {
            AlertDialogHandler.showConfirmOverwriteFile(context,
                    fileName,
                    locations,
                    transitions,
                    lines,
                    data);
        }
    }

    public static JSONObject createFileContent(ArrayList<Location> locations,
                                               ArrayList<Transition> transitions,
                                               CopyOnWriteArrayList<CPNLine> lines,
                                               DataTypeCollection data) {

        JSONObject object = new JSONObject();
        try {
            if (locations != null && !locations.isEmpty()) {
                JSONArray array = new JSONArray();
                for (Location location : locations) {
                    array.put(location.toJSON());
                }
                object.accumulate(Constants.JSON_LOCATIONS_KEY, array);
            }
            if (transitions != null && !transitions.isEmpty()) {
                JSONArray array = new JSONArray();
                for (Transition transition : transitions) {
                    array.put(transition.toJSON());
                }
                object.accumulate(Constants.JSON_TRANSITIONS_KEY, array);
            }
            if (lines != null && !lines.isEmpty()) {
                JSONArray array = new JSONArray();
                for (CPNLine line : lines) {
                    array.put(line.toJSON());
                }
                object.accumulate(Constants.JSON_LINES_KEY, array);
            }
            if (data != null) {
                object.accumulate(Constants.JSON_DATA_COLLECTION_KEY, data.toJSON());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public static void updateFile(String fileName,
                                  ArrayList<Location> locations,
                                  ArrayList<Transition> transitions,
                                  CopyOnWriteArrayList<CPNLine> lines,
                                  DataTypeCollection data) {
        preferences.edit().putString(fileName, createFileContent(locations, transitions, lines, data).toString()).apply();
    }

    public static Map<String, ?> getAllEntries() {
        return preferences.getAll();
    }

    public static String getDataFromFile(String fileName) {
        return preferences.getString(fileName, null);
    }

    public static ArrayList<Location> getLocationFromFile(JSONObject fileContent) {
        ArrayList<Location> locations = new ArrayList<>();
        try {
            JSONArray locationArray = (JSONArray) fileContent.get(Constants.JSON_LOCATIONS_KEY);
            for (int i = 0; i < locationArray.length(); i++) {
                JSONObject locationFromJSON = (JSONObject) locationArray.get(i);
                locations.add(extractLocation(locationFromJSON));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return locations;
    }

    public static ArrayList<Transition> getTransitionsFromFile(JSONObject fileContent) {
        ArrayList<Transition> transitions = new ArrayList<>();
        try {
            JSONArray transitionArray = (JSONArray) fileContent.get(Constants.JSON_TRANSITIONS_KEY);
            for (int i = 0; i < transitionArray.length(); i++) {
                JSONObject transitionFromJSON = (JSONObject) transitionArray.get(i);
                transitions.add(extractTransition(transitionFromJSON));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return transitions;
    }

    public static ArrayList<CPNLine> getCPNLinesFromFile(JSONObject fileContent) {
        ArrayList<CPNLine> lines = new ArrayList<>();
        try {
            JSONArray linesArray = (JSONArray) fileContent.get(Constants.JSON_LINES_KEY);
            for (int i = 0; i < linesArray.length(); i++) {
                JSONObject lineFromJSON = (JSONObject) linesArray.get(i);
                Location location;
                Transition transition;
                CPNLine cpnLine;
                boolean isStartLocation = lineFromJSON.getBoolean(CPNLine.JSON_IS_START_LOCATION_KEY);
                if (isStartLocation) {
                    location = extractLocation((JSONObject) lineFromJSON.get(CPNLine.JSON_START_VIEW_KEY));
                    transition = extractTransition((JSONObject) lineFromJSON.get(CPNLine.JSON_END_VIEW_KEY));
                    cpnLine = new CPNLine(location, transition, MainActivity.getToolbarHeight());
                } else {
                    transition = extractTransition((JSONObject) lineFromJSON.get(CPNLine.JSON_START_VIEW_KEY));
                    location = extractLocation((JSONObject) lineFromJSON.get(CPNLine.JSON_END_VIEW_KEY));
                    cpnLine = new CPNLine(transition, location, MainActivity.getToolbarHeight());
                }
                try {
                    JSONObject cpnExpressionJSON = lineFromJSON.getJSONObject(CPNLine.JSON_CPN_EXPRESSION_KEY);
                    String expression = cpnExpressionJSON.getString(CPNExpression.JSON_EXPRESSION_KEY);
                    DataTypeObjects values = new DataTypeObjects();
                    DataTypeObjects variables = new DataTypeObjects();
                    try {
                        values = new DataTypeObjects((TreeMap<String, Integer>) jsonToMap(cpnExpressionJSON.getJSONObject(CPNExpression.JSON_VALUES_KEY)));
                    } catch (JSONException ignored) {
                    }
                    try {
                        variables = new DataTypeObjects((TreeMap<String, Integer>) jsonToMap(cpnExpressionJSON.getJSONObject(CPNExpression.JSON_VARIABLES_KEY)));
                    } catch (JSONException ignored) {

                    }
                    CPNExpression cpnExpression = new CPNExpression(expression, values, variables);
                    cpnLine.setCpnExpression(cpnExpression);
                } catch (JSONException ignored) {

                }
                lines.add(cpnLine);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static DataTypeCollection getDataCollectionFromFile(JSONObject fileContent) {
        DataTypeCollection collection = new DataTypeCollection();
        try {
            JSONObject data = fileContent.getJSONObject(Constants.JSON_DATA_COLLECTION_KEY);
            Iterator<String> iter = data.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    JSONObject value = data.getJSONObject(key);
                    DataTypeObjects values = new DataTypeObjects((TreeMap<String, Integer>) jsonToMap(value.getJSONObject(DataTypeCollection.JSON_TYPES_KEY)));
                    DataTypeObjects variables = new DataTypeObjects((TreeMap<String, Integer>) jsonToMap(value.getJSONObject(DataTypeCollection.JSON_VARIABLES_KEY)));
                    collection.addNewConstants(key, values);
                    collection.addNewVariables(key, variables);
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return collection;
    }

    public static Map<String, Integer> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Integer> retMap = new TreeMap<>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Integer> toMap(JSONObject object) throws JSONException {
        Map<String, Integer> map = new TreeMap<>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Integer value = (Integer) object.get(key);

            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public static Location extractLocation(JSONObject object) throws JSONException {
        Location location = new Location(context, object.getString(Location.JSON_NAME_KEY));
        location.setX(object.getInt(Location.JSON_X_COORDINATE));
        location.setY(object.getInt(Location.JSON_Y_COORDINATE));
        if (object.has(Location.JSON_DATA_TYPE_NAME_KEY)) {
            location.setDataTypeName(object.getString(Location.JSON_DATA_TYPE_NAME_KEY));
        }
        if (object.has(Location.JSON_INITIAL_MARKING_KEY)) {
            location.setInitialMarking(new DataTypeObjects((TreeMap<String, Integer>) jsonToMap(object.getJSONObject(Location.JSON_INITIAL_MARKING_KEY))));
        }
        return location;
    }

    public static Transition extractTransition(JSONObject object) throws JSONException {
        Transition transition = new Transition(context, object.getString(Transition.JSON_NAME_KEY));
        transition.setX(object.getInt(Transition.JSON_X_COORDINATE));
        transition.setY(object.getInt(Transition.JSON_Y_COORDINATE));
        try {
            transition.onGuardExpressionSet(object.getString(Transition.JSON_GUARD_EXPRESSION_KEY));
        } catch (JSONException e) {

        }
        return transition;
    }
}
