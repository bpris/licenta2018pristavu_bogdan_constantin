package com.example.bogdan.cpnsimulator.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.CacheManager;
import com.example.bogdan.cpnsimulator.data.DataTypeCollection;
import com.example.bogdan.cpnsimulator.data.DataTypeObjects;
import com.example.bogdan.cpnsimulator.ui.customviews.BaseEntity;
import com.example.bogdan.cpnsimulator.ui.customviews.CPNLine;
import com.example.bogdan.cpnsimulator.ui.customviews.CustomHorizontalScrollView;
import com.example.bogdan.cpnsimulator.ui.customviews.CustomScrollView;
import com.example.bogdan.cpnsimulator.ui.customviews.DataTypeInputForm;
import com.example.bogdan.cpnsimulator.ui.customviews.DeclarationForm;
import com.example.bogdan.cpnsimulator.ui.customviews.DrawingBoard;
import com.example.bogdan.cpnsimulator.ui.customviews.InitialMarkingInputForm;
import com.example.bogdan.cpnsimulator.ui.customviews.InputForm;
import com.example.bogdan.cpnsimulator.ui.customviews.Location;
import com.example.bogdan.cpnsimulator.ui.customviews.Transition;
import com.example.bogdan.cpnsimulator.ui.customviews.VariableInputForm;
import com.example.bogdan.cpnsimulator.utils.Constants;
import com.example.bogdan.cpnsimulator.utils.MathUtils;
import com.example.bogdan.cpnsimulator.utils.AssignmentCalculator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class MainActivity extends Activity {

    private DrawerLayout sideMenu;
    private FrameLayout screenContainer;
    private Button openButton;
    private TextView addVariable;
    private TextView defineType;
    private TextView declarationsList;
    private TextView startSimulationScreen;
    private TextView addLocation;
    private TextView addTransition;
    private TextView drawLine;
    private TextView deleteLine;
    private TextView saveNetwork;
    private TextView loadNetwork;
    private TextView newNetwork;
    private TextView closeButton;
    private DrawingBoard drawingBoard;
    private LinearLayout toolbar;
    private ListView locationOptionsList;
    private ListView transitionOptionsList;
    private ListView arrowSelection;
    private InputForm inputForm;
    private VariableInputForm variableInputForm;
    private DataTypeInputForm dataTypeInputForm;
    private InitialMarkingInputForm initialMarkingInputForm;
    private DeclarationForm declarationForm;
    private static CustomScrollView verticalScrollView;
    private static CustomHorizontalScrollView horizontalScrollView;

    private boolean isDrawingLine;
    private boolean isDeleteModeOn;

    private String lastFileName;

    private Location locationConnection;
    private Transition transitionConnection;

    private Location lastPressedLocation;
    private Transition lastPressedTransition;

    private DataTypeCollection dataTypeCollection;

    private Resources resources;
    private CacheManager cacheManager;
    private AlertDialogHandler alertDialogHandler;

    private static int toolbarHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initData();
        prepareOptionLists();
        initButtonsListeners();
        alertDialogHandler = new AlertDialogHandler(this);
        //ExpressionParser.evaluate("a<4&&c>5", new String[]{"a", "c"}, new String[]{"1", "7"});
        //ExpressionParser.isExpressionValid(expr1, type, "a");
    }

    private void initViews() {
        sideMenu = findViewById(R.id.drawer_layout);
        sideMenu.setScrimColor(Color.TRANSPARENT);
        sideMenu.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        sideMenu.openDrawer(Gravity.START);
        screenContainer = findViewById(R.id.screen_container);
        openButton = findViewById(R.id.open_menu);
        addVariable = findViewById(R.id.add_variables_type);
        defineType = findViewById(R.id.add_data_type);
        declarationsList = findViewById(R.id.see_declarations);
        startSimulationScreen = findViewById(R.id.start_simulation);
        addLocation = findViewById(R.id.btn_add_location);
        addTransition = findViewById(R.id.btn_add_transition);
        drawLine = findViewById(R.id.btn_add_line);
        deleteLine = findViewById(R.id.btn_delete_item);
        saveNetwork = findViewById(R.id.save_button);
        loadNetwork = findViewById(R.id.load_button);
        newNetwork = findViewById(R.id.new_net_button);
        closeButton = findViewById(R.id.close_button);
        drawingBoard = findViewById(R.id.drawing_board);
        toolbar = findViewById(R.id.toolbar);
        transitionOptionsList = findViewById(R.id.lv_transition_option);
        locationOptionsList = findViewById(R.id.lv_locations_option);
        arrowSelection = findViewById(R.id.lv_arrow_selection);
        verticalScrollView = findViewById(R.id.vertical_scrollable_surface);
        horizontalScrollView = findViewById(R.id.horizontal_scrollable_surface);
        horizontalScrollView.getScrollX();
    }

    private void initData() {
        this.resources = getResources();
        isDrawingLine = false;
        isDeleteModeOn = false;
        dataTypeCollection = new DataTypeCollection();
        cacheManager = new CacheManager(this);
        toolbarHeight = toolbar.getHeight();
        lastFileName = "";
    }

    private void prepareOptionLists() {
        ArrayList<String> locationOptions = new ArrayList<>();
        locationOptions.add(resources.getString(R.string.change_name_option));
        locationOptions.add(resources.getString(R.string.define_type_option));
        locationOptions.add(resources.getString(R.string.initial_marking_option));
        locationOptions.add(resources.getString(R.string.close_option));

        ArrayList<String> transitionOptions = new ArrayList<>();
        transitionOptions.add(resources.getString(R.string.change_name_option));
        transitionOptions.add(resources.getString(R.string.guard_expression_option));
        transitionOptions.add(resources.getString(R.string.possible_assignments_option));
        transitionOptions.add(resources.getString(R.string.close_option));

        locationOptionsList.setAdapter(new OptionListAdapter(this, locationOptions));
        transitionOptionsList.setAdapter(new OptionListAdapter(this, transitionOptions));

        disableScrollForListView(locationOptionsList);
        disableScrollForListView(transitionOptionsList);
    }

    private void disableScrollForListView(ListView listView) {
        listView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
    }

    public void initListenersForLocationOrTransition(View view) {
        view.setOnTouchListener(new View.OnTouchListener() {
            long lastClickTime = 0;
            private int xDelta;
            private int yDelta;

            @Override
            public boolean onTouch(View view, MotionEvent event) {

                clearExpressionFocus(view);

                moveElementsIfConnected(view);

                final int xPos = (int) event.getRawX();
                final int yPos = (int) event.getRawY();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        if (isDeleteModeOn) {
                            onDeleteItem(view);
                            break;
                        }
                        FrameLayout.LayoutParams lParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                        xDelta = xPos - lParams.leftMargin;
                        yDelta = yPos - lParams.topMargin;
                        if (!isDrawingLine) {
                            long clickTime = System.currentTimeMillis();
                            if (clickTime - lastClickTime < Constants.DOUBLE_CLICK_DELAY_MILLS) {
                                onDoubleClick(view);
                            }
                            lastClickTime = clickTime;
                        } else {
                            onSingleClick(view);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        //hideBothLists();
                        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                        layoutParams.leftMargin = xPos - xDelta;
                        layoutParams.topMargin = yPos - yDelta;
                        if (layoutParams.leftMargin >= drawingBoard.getWidth() - view.getWidth()) {
                            layoutParams.leftMargin = drawingBoard.getWidth() - view.getWidth();
                        }
                        if (layoutParams.leftMargin < 0) {
                            layoutParams.leftMargin = 0;
                        }
                        if (layoutParams.topMargin >= drawingBoard.getHeight() - view.getHeight()) {
                            layoutParams.topMargin = drawingBoard.getHeight() - view.getHeight();
                        }
                        if (layoutParams.topMargin < 0) {
                            layoutParams.topMargin = 0;
                        }
                        view.setLayoutParams(layoutParams);
                        if (view instanceof Location) {
                            if (lastPressedLocation != null && lastPressedLocation.equals(view)) {
                                moveList(locationOptionsList, view);
                            }
                        } else {
                            if (lastPressedTransition != null && lastPressedTransition.equals(view)) {
                                moveList(transitionOptionsList, view);
                            }
                        }
                        break;
                }
                return true;
            }

            private void onDoubleClick(View view) {
                prepareOptionLists();
                closeLastItems();
                if (view instanceof Location) {
                    if (lastPressedLocation == null || !view.equals(lastPressedLocation)) {
                        hideOptionListView(transitionOptionsList);
                        showOptionListView(locationOptionsList, view, true);
                        lastPressedLocation = (Location) view;
                        handleLocationOptionClick((Location) view);
                    } else {
                        hideBothLists();
                    }
                } else {
                    if (lastPressedTransition == null || !view.equals(lastPressedTransition)) {
                        hideOptionListView(locationOptionsList);
                        showOptionListView(transitionOptionsList, view, false);
                        lastPressedTransition = (Transition) view;
                        handleTransitionOptionClick((Transition) view);
                    } else {
                        hideBothLists();
                    }
                }
            }

            private void closeLastItems() {
                if (lastPressedTransition != null) {
                    lastPressedTransition.forceCloseAll();
                }
                if (lastPressedLocation != null) {
                    lastPressedLocation.forceCloseAll();
                }
            }

            private void onSingleClick(View view) {
                hideBothLists();
                if (isDrawingLine) {
                    if (view instanceof Location) {
                        locationConnection = (Location) view;
                        if (transitionConnection != null) {
                            onEndsSelected(transitionConnection, locationConnection);
                        }
                    } else {
                        transitionConnection = (Transition) view;
                        if (locationConnection != null) {
                            onEndsSelected(locationConnection, transitionConnection);
                        }
                    }
                }
            }
        });
    }


    public void makeValidations(Location location) {
        Iterator it = drawingBoard.getLines().iterator();
        while (it.hasNext()) {
            CPNLine line = (CPNLine) it.next();
            if (location != null) {
                if (line.getStartView().getName().equals(location.getName())) {
                    drawNewLine(location, line.getEndView());
                } else if (line.getEndView().getName().equals(location.getName())) {
                    drawNewLine(line.getStartView(), location);
                }
            }
        }
    }

    private void initButtonsListeners() {

        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.openDrawer(Gravity.START);
            }
        });

        addVariable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dataTypeCollection.getTypes() != null && dataTypeCollection.getTypes().isEmpty()) {
                    alertDialogHandler.showDialog(MainActivity.this, "You must first create a data type in order to use this option");
                    return;
                }
                sideMenu.closeDrawer(Gravity.START);
                addVariables();
            }
        });

        defineType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.closeDrawer(Gravity.START);
                defineDataType();
            }
        });

        declarationsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dataTypeCollection.getTypes() != null && dataTypeCollection.getTypes().isEmpty()) {
                    alertDialogHandler.showDialog(MainActivity.this, "You must first create a data type in order to use this option");
                    return;
                }
                sideMenu.closeDrawer(Gravity.START);
                editExistingDataTypes();
            }
        });

        startSimulationScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SimulationActivity.class);
                intent.putExtra(Constants.INTENT_KEY, CacheManager.createFileContent(
                        drawingBoard.getLocations(),
                        drawingBoard.getTransitions(),
                        drawingBoard.getLines(),
                        dataTypeCollection).toString());
                startActivity(intent);
                sideMenu.closeDrawer(Gravity.START);
            }
        });

        addLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.closeDrawer(Gravity.START);
                drawingBoard.addBaseEntityView(true);
            }
        });

        addTransition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.closeDrawer(Gravity.START);
                drawingBoard.addBaseEntityView(false);
            }
        });

        drawLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.closeDrawer(Gravity.START);
                setDeleteButtonText();
                isDeleteModeOn = false;
                if (isDrawingLine) {
                    isDrawingLine = false;
                    drawLine.setBackground(resources.getDrawable(R.drawable.arrow));
                    locationConnection = null;
                    transitionConnection = null;
                } else {
                    onDrawLineClicked();
                }
            }
        });

        deleteLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.closeDrawer(Gravity.START);
                hideBothLists();
                isDrawingLine = false;
                drawLine.setBackground(resources.getDrawable(R.drawable.arrow));
                if (!isDeleteModeOn) {
                    deleteLine.setBackground(null);
                    deleteLine.setText(resources.getString(R.string.cancel_text));
                    ViewGroup.LayoutParams params = deleteLine.getLayoutParams();
                    params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                    deleteLine.setLayoutParams(params);
                    isDeleteModeOn = true;
                } else {
                    setDeleteButtonText();
                    isDeleteModeOn = false;
                }
            }
        });

        saveNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.closeDrawer(Gravity.START);
                alertDialogHandler.showFileNameInputDialog(MainActivity.this,
                        drawingBoard.getLocations(),
                        drawingBoard.getTransitions(),
                        drawingBoard.getLines(),
                        dataTypeCollection);
            }
        });

        loadNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.closeDrawer(Gravity.START);
                alertDialogHandler.showFileChoosing(MainActivity.this, CacheManager.getAllEntries());
            }
        });

        newNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialogHandler.showNewNetConfirmScreen(MainActivity.this);
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sideMenu.closeDrawer(Gravity.START);
            }
        });

    }

    public void createNewNet() {
        sideMenu.closeDrawer(Gravity.START);
        hideInputForm();
        hideBothLists();
        clearData();
        lastFileName = "";
    }

    private void defineDataType() {
        //showInputForm(null, 1);
        showDataTypeInputForm();
    }

    private void editExistingDataTypes() {
        //showInputForm(null, 2);
        showDeclarationsForm();
    }

    private void addVariables() {
        //showInputForm(null, 3);
        showVariableInputForm();
    }

    public boolean clearExpressionFocus(View view) {
        if (drawingBoard.getFocusedChild() != null && drawingBoard.getFocusedChild() instanceof EditText) {
            drawingBoard.getFocusedChild().clearFocus();
            drawingBoard.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        return false;
    }


    private void showDialogForAssignments(List<CPNLine> lines, String guardExpression) {
        Set<Map<String, String>> possibleAssignments = AssignmentCalculator.getCommonAssignments(lines, guardExpression);

        alertDialogHandler.showListDialog(this, possibleAssignments);
    }

    public void drawNewLine(BaseEntity start, BaseEntity end) {
        CPNLine cpnLine = new CPNLine(start, end, toolbar.getHeight());
        String lastValue = null;

        if (drawingBoard.getLines().size() != 0 && drawingBoard.getLines().contains(cpnLine)) {
            CPNLine toBeReplacedLine = drawingBoard.getLines().get(drawingBoard.getLines().indexOf(cpnLine));
            if (toBeReplacedLine.getCPNExpression() != null &&
                    toBeReplacedLine.getCPNExpression().getExpression() != null
                    && !toBeReplacedLine.getCPNExpression().getExpression().isEmpty()) {
                lastValue = toBeReplacedLine.getExpressionInput().getText().toString();
            }
            drawingBoard.removeView(toBeReplacedLine.getExpressionInput());
            drawingBoard.getLines().remove(toBeReplacedLine);
        }
        drawingBoard.getLines().add(cpnLine);

        drawingBoard.draw(Color.BLACK);
        EditText expression = (EditText) getLayoutInflater().inflate(R.layout.arrow_expression_input, drawingBoard, false);
        DataTypeObjects types;
        DataTypeObjects variables;
        if (start instanceof Location) {
            types = ((Location) start).getInitialMarking();
            variables = dataTypeCollection.getVariablesForKey(((Location) start).getDataTypeName());
        } else {
            types = ((Location) end).getInitialMarking();
            variables = dataTypeCollection.getVariablesForKey(((Location) end).getDataTypeName());
        }
        cpnLine.setExpressionInput(expression, lastValue, types, variables);
        drawingBoard.addView(cpnLine.getExpressionInput(), 0);
        redrawViewOnArrow(cpnLine.getExpressionInput(), cpnLine);
    }

    private void onDrawLineClicked() {
        isDrawingLine = true;
        locationConnection = null;
        transitionConnection = null;

        drawLine.setBackground(resources.getDrawable(android.R.drawable.ic_menu_close_clear_cancel));
    }

    private void onEndsSelected(BaseEntity start, BaseEntity end) {
        drawNewLine(start, end);
        isDrawingLine = false;
        locationConnection = null;
        transitionConnection = null;

        drawLine.setBackground(resources.getDrawable(R.drawable.arrow));
    }

    private void onDeleteItem(View view) {
        for (int i = 0; i < drawingBoard.getLines().size(); i++) {
            CPNLine line = drawingBoard.getLines().get(i);
            if (line.getStartView().equals(view) || line.getEndView().equals(view)) {
                drawingBoard.removeView(drawingBoard.getLines().get(i).getExpressionInput());
                drawingBoard.getLines().remove(i--);
            }
        }
        drawingBoard.removeView(view);
        if (view instanceof Location) {
            drawingBoard.getLocations().remove(view);
        } else if (view instanceof Transition) {
            drawingBoard.getTransitions().remove(view);
        }
        isDeleteModeOn = false;
        setDeleteButtonText();
    }

    private void moveElementsIfConnected(View view) {
        if (drawingBoard.getLines().size() != 0) {
            for (int i = 0; i < drawingBoard.getLines().size(); i++) {
                if (view.equals(drawingBoard.getLines().get(i).getStartView())) {
                    drawNewLine((BaseEntity) view, (BaseEntity) drawingBoard.getLines().get(i).getEndView());
                    redrawViewOnArrow(arrowSelection, drawingBoard.getLines().get(i));
                }
                if (view.equals(drawingBoard.getLines().get(i).getEndView())) {
                    drawNewLine((BaseEntity) drawingBoard.getLines().get(i).getStartView(), (BaseEntity) view);
                    redrawViewOnArrow(arrowSelection, drawingBoard.getLines().get(i));
                }
            }
        }
    }

    private void showOptionListView(ListView listView, View anchor, boolean isLocationOption) {
        listView.setVisibility(View.VISIBLE);
        if (isLocationOption) {
            lastPressedLocation = (Location) anchor;
        } else {
            lastPressedTransition = (Transition) anchor;
        }
        moveList(listView, anchor);
    }

    private void hideOptionListView(ListView listView) {
        listView.setVisibility(View.GONE);
        if (listView.equals(locationOptionsList)) {
            lastPressedLocation = null;
        } else {
            lastPressedTransition = null;
        }
    }

    private void hideBothLists() {
        hideOptionListView(locationOptionsList);
        hideOptionListView(transitionOptionsList);
    }

    private void showVariableInputForm() {
        verticalScrollView.setVisibility(View.GONE);
        variableInputForm = new VariableInputForm(this, dataTypeCollection);
        variableInputForm.setVisibility(View.VISIBLE);
        screenContainer.addView(variableInputForm, screenContainer.getChildCount());
        toolbar.setVisibility(View.INVISIBLE);
    }

    private void showDataTypeInputForm() {
        verticalScrollView.setVisibility(View.GONE);
        dataTypeInputForm = new DataTypeInputForm(this, dataTypeCollection);
        dataTypeInputForm.setVisibility(View.VISIBLE);
        screenContainer.addView(dataTypeInputForm, screenContainer.getChildCount());
        toolbar.setVisibility(View.INVISIBLE);
    }

    private void showInitialMarkingForm(Location location) {
        verticalScrollView.setVisibility(View.GONE);
        initialMarkingInputForm = new InitialMarkingInputForm(this, location, dataTypeCollection);
        initialMarkingInputForm.setVisibility(View.VISIBLE);
        screenContainer.addView(initialMarkingInputForm, screenContainer.getChildCount());
        toolbar.setVisibility(View.INVISIBLE);
    }

    private void showDeclarationsForm() {
        verticalScrollView.setVisibility(View.GONE);
        declarationForm = new DeclarationForm(this, dataTypeCollection);
        declarationForm.setVisibility(View.VISIBLE);
        screenContainer.addView(declarationForm, screenContainer.getChildCount());
        toolbar.setVisibility(View.INVISIBLE);
    }

    public void hideVariableInputForm() {
        verticalScrollView.setVisibility(View.VISIBLE);
        screenContainer.removeView(variableInputForm);
        variableInputForm = null;
        toolbar.setVisibility(View.VISIBLE);
    }

    public void hideConstantsInputForm() {
        verticalScrollView.setVisibility(View.VISIBLE);
        screenContainer.removeView(dataTypeInputForm);
        dataTypeInputForm = null;
        toolbar.setVisibility(View.VISIBLE);
    }

    public void hideInitialMarkingForm() {
        verticalScrollView.setVisibility(View.VISIBLE);
        screenContainer.removeView(initialMarkingInputForm);
        initialMarkingInputForm = null;
        toolbar.setVisibility(View.VISIBLE);
    }

    public void hideDeclarationsForm() {
        verticalScrollView.setVisibility(View.VISIBLE);
        screenContainer.removeView(declarationForm);
        declarationForm = null;
        toolbar.setVisibility(View.VISIBLE);
    }

    private void showInputForm(Location location, int option) {
        if (inputForm != null) {
            hideInputForm();
        }
        verticalScrollView.setVisibility(View.GONE);
        inputForm = new InputForm(this, location, dataTypeCollection, option);
        inputForm.setVisibility(View.VISIBLE);
        inputForm.initListeners();
        screenContainer.addView(inputForm, screenContainer.getChildCount());
    }

    public void hideInputForm() {
        verticalScrollView.setVisibility(View.VISIBLE);
        screenContainer.removeView(inputForm);
        inputForm = null;
    }

    private void moveList(ListView listView, View anchor) {
        if (listView.getVisibility() == View.VISIBLE) {
            int[] coord = new int[2];
            anchor.getLocationOnScreen(coord);
            listView.measure(View.MeasureSpec.EXACTLY, View.MeasureSpec.EXACTLY);
            if (MathUtils.getMax(listView.getMeasuredWidth(), listView.getWidth()) >= coord[0]) {
                listView.setX(coord[0] + resources.getDimension(R.dimen.bu_5));
            } else {
                listView.setX(coord[0] - MathUtils.getMax(listView.getMeasuredWidth(), listView.getWidth()));
            }
            if (listView.getHeight() >= drawingBoard.getHeight() - coord[1] - anchor.getHeight()) {
                listView.setY(coord[1] - toolbar.getHeight() - listView.getHeight() - anchor.getHeight());
            } else {
                listView.setY(coord[1] - toolbar.getHeight());
            }
        }
    }

    private void handleLocationOptionClick(final Location location) {
        locationOptionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView optionName = view.findViewById(R.id.option_name);
                switch (i) {
                    case 0:
                        if (!location.isEditing()) {
                            optionName.setText(resources.getString(R.string.finish_option));
                            location.changeName();
                        } else {
                            if (isNameUsed(location)) {
                                alertDialogHandler.showDialog(MainActivity.this, "Name is already used, please select a new one");
                            } else {
                                optionName.setText(resources.getString(R.string.change_name_option));
                                location.onNameChanged();
                            }
                        }
                        break;
                    case 1:
                        if (dataTypeCollection.getTypes() != null && dataTypeCollection.getTypes().isEmpty()) {
                            alertDialogHandler.showDialog(MainActivity.this, "You must first create a data type in order to use this option");
                            return;
                        }
                        hideOptionListView(locationOptionsList);
                        //showInputForm(location, 4);
                        selectDataTypeForLocation(location);
                        break;
                    case 2:
                        hideOptionListView(locationOptionsList);
                        if (location.getDataTypeName() != null && !location.getDataTypeName().isEmpty()) {
                            //showInputForm(location, 5);
                            showInitialMarkingForm(location);
                        } else {
                            Toast.makeText(MainActivity.this, "Please select a type for this location", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 3:
                        location.setEditing(false);
                        hideOptionListView(locationOptionsList);
                        break;
                }
            }
        });
    }

    private void handleTransitionOptionClick(final Transition transition) {
        transitionOptionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView optionName = view.findViewById(R.id.option_name);
                switch (i) {
                    case 0:
                        if (!transition.isEditing()) {
                            optionName.setText(resources.getString(R.string.finish_option));
                            transition.changeName();
                        } else {
                            if (isNameUsed(transition)) {
                                alertDialogHandler.showDialog(MainActivity.this, "Name is already used, please select a new one");
                            } else {
                                optionName.setText(resources.getString(R.string.change_name_option));
                                transition.onNameChanged();
                            }
                        }
                        break;
                    case 1:
                        hideOptionListView(locationOptionsList);
                        alertDialogHandler.showInputDialog(MainActivity.this, transition);
                        break;
                    case 2:
                        hideOptionListView(locationOptionsList);
                        List<CPNLine> inputLines = new ArrayList<>();
                        String guardExpression = null;
                        for (CPNLine line : drawingBoard.getLines()) {
                            if (line.getEndView().equals(transition)) {
                                inputLines.add(line);
                                guardExpression = ((Transition) line.getEndView()).getGuardExpression();
                            }
                        }
                        showDialogForAssignments(inputLines, guardExpression);
                        break;
                    case 3:
                        transition.setEditing(false);
                        hideOptionListView(transitionOptionsList);
                        break;
                }
            }
        });
    }

    private void selectDataTypeForLocation(Location location) {
        AlertDialogHandler.selectDataTypeForLocationDialog(this, dataTypeCollection.getDataTypeNames(), location);
    }

    public void setDataTypeForLocation(Location location, String dataTypeName) {
        location.setDataTypeName(dataTypeName);
        location.setInitialMarking(dataTypeCollection.getConstantsForKey(dataTypeName));
        for(CPNLine line : drawingBoard.getLines()){
            if(line.getStartView().equals(location)){
                drawNewLine(location, line.getEndView());
            }
            if(line.getEndView().equals(location)){
                drawNewLine(line.getStartView(), location);
            }
        }
    }

    private boolean isNameUsed(BaseEntity element) {
        for (Location location : drawingBoard.getLocations()) {
            if (location.getName().equals(element.getEditViewName().getText().toString()) && !location.equals(element)) {
                return true;
            }
        }
        for (Transition transition : drawingBoard.getTransitions()) {
            if (transition.getName().equals(element.getEditViewName().getText().toString()) && !transition.equals(element)) {
                return true;
            }
        }
        return false;
    }

    public void setDeleteButtonText() {
        deleteLine.setText("");
        ViewGroup.LayoutParams params = deleteLine.getLayoutParams();
        params.width = (int) resources.getDimension(R.dimen.bu_3);
        deleteLine.setLayoutParams(params);
        deleteLine.setBackground(resources.getDrawable(R.drawable.eraser));
    }

    public boolean isDeleteModeOn() {
        return isDeleteModeOn;
    }

    public void setDeleteMode(boolean deleteModeOn) {
        isDeleteModeOn = deleteModeOn;
    }

    private void redrawViewOnArrow(View view, CPNLine line) {
        view.setX(line.getExpressionLocation().x);
        view.setY(line.getExpressionLocation().y);
        float angle = (float) line.getAngle();
        if (angle >= 90 && angle <= 180) {
            angle -= 180;
        } else {
            if (angle <= -90 && angle >= -180) {
                angle += 180;
            }
        }
        //view.setRotation(angle);
    }

    private void clearData() {
        drawingBoard.removeAllViews();
        drawingBoard.setLines(new CopyOnWriteArrayList<CPNLine>());
        drawingBoard.setLocations(new ArrayList<Location>());
        drawingBoard.setTransitions(new ArrayList<Transition>());
        this.dataTypeCollection = new DataTypeCollection();
    }

    public void loadNetwork(final JSONObject fileContent) {
        clearData();
        drawingBoard.addView(locationOptionsList);
        drawingBoard.addView(transitionOptionsList);
        drawingBoard.addView(arrowSelection);

        ArrayList<Location> locations = CacheManager.getLocationFromFile(fileContent);
        if (locations != null) {
            drawingBoard.setNumberOfLocations(locations.size());
            for (int i = 0; i < locations.size(); i++) {
                Location location = new Location(this, locations.get(i).getName());
                drawingBoard.getLocations().add(location);
                drawingBoard.addView(location, 0, new ViewGroup.LayoutParams(
                        (int) resources.getDimension(R.dimen.bu_5),
                        (int) resources.getDimension(R.dimen.bu_5)));
                initListenersForLocationOrTransition(drawingBoard.getLocations().get(i));
                if (locations.get(i).getDataTypeName() != null) {
                    drawingBoard.getLocations().get(i).setDataTypeName(locations.get(i).getDataTypeName());
                }
                if (locations.get(i).getInitialMarking() != null) {
                    drawingBoard.getLocations().get(i).setInitialMarking(locations.get(i).getInitialMarking());
                }
                MotionEvent motionEvent = MotionEvent.obtain(
                        0,
                        0,
                        MotionEvent.ACTION_MOVE,
                        locations.get(i).getX(),
                        locations.get(i).getY(),
                        0
                );
                drawingBoard.getLocations().get(i).dispatchTouchEvent(motionEvent);
            }
        }

        ArrayList<Transition> transitions = CacheManager.getTransitionsFromFile(fileContent);
        if (transitions != null) {
            drawingBoard.setNumberOfTransitions(transitions.size());
            for (int i = 0; i < transitions.size(); i++) {
                Transition transition = new Transition(this, transitions.get(i).getName());
                drawingBoard.getTransitions().add(transition);
                drawingBoard.addView(transition, 0, new ViewGroup.LayoutParams(
                        (int) resources.getDimension(R.dimen.bu_5),
                        (int) resources.getDimension(R.dimen.bu_5)));
                initListenersForLocationOrTransition(drawingBoard.getTransitions().get(i));
                if (transitions.get(i).getGuardExpression() != null) {
                    drawingBoard.getTransitions().get(i).onGuardExpressionSet(transitions.get(i).getGuardExpression());
                }
                MotionEvent motionEvent = MotionEvent.obtain(
                        0,
                        0,
                        MotionEvent.ACTION_MOVE,
                        transitions.get(i).getX(),
                        transitions.get(i).getY(),
                        0
                );
                drawingBoard.getTransitions().get(i).dispatchTouchEvent(motionEvent);
            }
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ArrayList<CPNLine> cpnLines = CacheManager.getCPNLinesFromFile(fileContent);
                if (cpnLines != null) {
                    for (int i = 0; i < cpnLines.size(); i++) {
                        CPNLine cpnLine = cpnLines.get(i);
                        if (cpnLine.getStartView() instanceof Location) {
                            drawingBoard.getLocations().contains(cpnLine.getStartView());
                            drawingBoard.getTransitions().contains(cpnLine.getEndView());
                            onEndsSelected(
                                    drawingBoard.getLocations().get(drawingBoard.getLocations().indexOf(cpnLine.getStartView())),
                                    drawingBoard.getTransitions().get(drawingBoard.getTransitions().indexOf(cpnLine.getEndView())));
                        } else {
                            drawingBoard.getTransitions().contains(cpnLine.getStartView());
                            drawingBoard.getLocations().contains(cpnLine.getEndView());
                            onEndsSelected(
                                    drawingBoard.getTransitions().get(drawingBoard.getTransitions().indexOf(cpnLine.getStartView())),
                                    drawingBoard.getLocations().get(drawingBoard.getLocations().indexOf(cpnLine.getEndView())));
                        }
                        if (cpnLines.get(i).getCPNExpression() != null) {
                            drawingBoard.getLines().get(i).setExpressionInput(
                                    drawingBoard.getLines().get(i).getExpressionInput(),
                                    cpnLines.get(i).getCPNExpression().getExpression(),
                                    drawingBoard.getLines().get(i).getCPNExpression().getValues(),
                                    drawingBoard.getLines().get(i).getCPNExpression().getVariables());
                        }
                    }
                }
            }
        }, 100);
        drawingBoard.draw(Color.BLACK);
        this.dataTypeCollection = CacheManager.getDataCollectionFromFile(fileContent);
    }

    public void setDataTypeCollection(DataTypeCollection dataTypeCollection) {
        this.dataTypeCollection = dataTypeCollection;
    }

    public static int getToolbarHeight() {
        return toolbarHeight;
    }

    public static int getHorizontalScrollDistance() {
        return horizontalScrollView.getScrollX();
    }

    public static int getVerticalScrollDistance() {
        return verticalScrollView.getScrollY();
    }

    public void setLastFileName(String fileName) {
        this.lastFileName = fileName;
    }

    public String getLastFileName() {
        return this.lastFileName;
    }

    @Override
    public void onBackPressed() {
        if (dataTypeInputForm != null) {
            hideConstantsInputForm();
        } else if (variableInputForm != null) {
            hideVariableInputForm();
        } else if (initialMarkingInputForm != null) {
            hideInitialMarkingForm();
        } else if (declarationForm != null) {
            hideDeclarationsForm();
        } else if (locationOptionsList.getVisibility() == View.VISIBLE) {
            hideOptionListView(locationOptionsList);
        } else if (transitionOptionsList.getVisibility() == View.VISIBLE) {
            hideOptionListView(transitionOptionsList);
        } else {
            AlertDialogHandler.showConfirmActionWithoutSave(this);
        }
    }
}
