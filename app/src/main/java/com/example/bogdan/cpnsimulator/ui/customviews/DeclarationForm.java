package com.example.bogdan.cpnsimulator.ui.customviews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.DataTypeCollection;

import java.util.ArrayList;
import java.util.Collections;

public class DeclarationForm extends LinearLayout {

    private LinearLayout inputZone;

    private DataTypeCollection data;
    private Context context;

    public DeclarationForm(Context context) {
        super(context);
    }

    public DeclarationForm(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DeclarationForm(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DeclarationForm(Context context, DataTypeCollection data) {
        super(context);
        this.context = context;
        this.data = data;
        initViews();
        populateWithData();
    }

    private void initViews() {
        inflateLayout();
        inputZone = findViewById(R.id.ll_input_zone);
    }

    private void inflateLayout() {
        inflate(context, R.layout.declaration_layout, this);
    }

    private void populateWithData() {
        ArrayList<String> dataTypeValues = new ArrayList<>();
        for (String s : data.getDataTypeNames()) {
            for (String value : data.getTypes().get(s).getObjectNames()) {
                if (isNumeric(value)) {
                    ArrayList<Integer> temp = getIntegerArray(data.getTypes().get(s).getObjectNames());
                    dataTypeValues.add(String.valueOf(Collections.min(temp)));
                    dataTypeValues.add(String.valueOf(Collections.max(temp)));
                    inflateRow(s, dataTypeValues, true);
                    break;
                } else {
                    dataTypeValues.add(value);
                    inflateRow(s, dataTypeValues, false);
                }
                dataTypeValues.clear();
            }
            dataTypeValues.clear();
            for (String variable : data.getVariables().get(s).getObjectNames()) {
                dataTypeValues.add(variable);
                inflateRow(s, dataTypeValues, false);
                dataTypeValues.clear();
            }
        }
    }

    private void inflateRow(String dataTypeName, ArrayList<String> values, final boolean isInteger) {
        final LinearLayout input = (LinearLayout) inflate(context, R.layout.declaration_row, null);
        final LinearLayout stringInput = input.findViewById(R.id.string_input_row);
        final LinearLayout integerInput = input.findViewById(R.id.integer_input_row);
        final TextView dataTypeNameInput = input.findViewById(R.id.data_type_name);
        final TextView stringValueInput = input.findViewById(R.id.string_value_input);
        final TextView minIntegerInput = input.findViewById(R.id.min_interval);
        final TextView maxIntegerInput = input.findViewById(R.id.max_interval);

        if (isInteger) {
            integerInput.setVisibility(VISIBLE);
            stringInput.setVisibility(GONE);
        } else {
            integerInput.setVisibility(GONE);
            stringInput.setVisibility(VISIBLE);
        }

        if (dataTypeName != null && !dataTypeName.isEmpty()) {
            dataTypeNameInput.setText(dataTypeName);
            if (isInteger) {
                minIntegerInput.setText(values.get(0));
                maxIntegerInput.setText(values.get(1));
            } else {
                stringValueInput.setText(values.get(0));
            }
        }
        inputZone.addView(input);
    }

    private boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private ArrayList<Integer> getIntegerArray(ArrayList<String> stringArray) {
        ArrayList<Integer> result = new ArrayList<>();
        for (String stringValue : stringArray) {
            try {
                //Convert String to Integer, and store it into integer array list.
                result.add(Integer.parseInt(stringValue));
            } catch (NumberFormatException nfe) {
                //System.out.println("Could not parse " + nfe);
            }
        }
        return result;
    }
}
