package com.example.bogdan.cpnsimulator.utils;

import com.example.bogdan.cpnsimulator.data.DataTypeObjects;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by bpristavu on 2/9/2018.
 */

public class ExpressionUtils {
    private static String STATEMENT_REGEX = "\\d+'[a-z][a-z]*";
    private static String LOWER_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*<[a-z]+\\d*[a-z]*";
    private static String LOWER_OR_EQUAL_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*<=[a-z]+\\d*[a-z]*";
    private static String GREATER_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*>[a-z]+\\d*[a-z]*";
    private static String GREATER_OR_EQUAL_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*<[a-z]+\\d*[a-z]*";
    private static String EQUAL_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*==[a-z]+\\d*[a-z]*";
    private static String NOT_EQUAL_INTERROGATION_REGEX = "[a-z]+\\d*[a-z]*!=[a-z]+\\d*[a-z]*";

    public static boolean hasInterrogation(String expression) {
        return expression.contains(Constants.THEN_SIGN)
                && expression.contains(Constants.ELSE_SIGN)
                && expression.indexOf(Constants.THEN_SIGN) > 0
                && expression.indexOf(Constants.ELSE_SIGN) < expression.length();
    }

    public static String getInterrogationFromExpression(String expression) {
        return expression.substring(0, expression.indexOf(Constants.THEN_SIGN));
    }

    public static String getThenStatementFromExpression(String expression) {
        return expression.substring(expression.indexOf(Constants.THEN_SIGN) + 1, expression.indexOf(Constants.ELSE_SIGN));
    }

    public static String getElseStatementFromExpression(String expression) {
        return expression.substring(expression.indexOf(Constants.ELSE_SIGN) + 1, expression.length());
    }

    public static DataTypeObjects getSpecificObjectsFromInterrogation(String interrogation, DataTypeObjects definedObjects) {
        return removeFromDefinedObjects(getAllObjectsFromInterrogation(interrogation), definedObjects);
    }

    public static DataTypeObjects getSpecificObjectsFromStatement(String statement, DataTypeObjects definedObjects) {
        return removeFromDefinedObjects(getAllObjectsFromStatement(statement), definedObjects);
    }


    private static boolean areObjectsInDataType(DataTypeObjects input, DataTypeObjects definedValues, DataTypeObjects definedVariables) {
        if (definedValues == null || definedVariables == null || definedValues.getObjects().isEmpty() || definedVariables.getObjects().isEmpty()) {
            return false;
        }

        if (input.getObjects().isEmpty()) {
            return false;
        }
        for (String key : input.getObjects().keySet()) {
            if (!definedValues.getObjects().containsKey(key) && !definedVariables.getObjects().containsKey(key)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isExpressionValid(String expression, DataTypeObjects definedValues, DataTypeObjects definedVariables) {
        if (hasInterrogation(expression)) {
            return isInterrogationValid(getInterrogationFromExpression(expression)) &&
                    isStatementValid(getThenStatementFromExpression(expression), definedValues, definedVariables) &&
                    isStatementValid(getElseStatementFromExpression(expression), definedValues, definedVariables) &&
                    areObjectsInDataType(getAllObjectsFromInterrogation(getInterrogationFromExpression(expression)), definedValues, definedVariables) &&
                    areObjectsInDataType(getAllObjectsFromStatement(getThenStatementFromExpression(expression)), definedValues, definedVariables) &&
                    areObjectsInDataType(getAllObjectsFromStatement(getThenStatementFromExpression(expression)), definedValues, definedVariables);
        } else {
            return isStatementValid(expression, definedValues, definedVariables);
        }
    }

    public static boolean isInputExpressionValid(String expression, DataTypeObjects definedValues, DataTypeObjects definedVariables) {
        if (hasInterrogation(expression)) {
            return isExpressionValid(expression, definedValues, definedVariables) &&
                    !statementHasVariables(getThenStatementFromExpression(expression), definedVariables) &&
                    !statementHasVariables(getElseStatementFromExpression(expression), definedVariables);
        } else {
            return isStatementValid(expression, definedValues, definedVariables);
        }
    }

    public static boolean isStatementValid(String expression, DataTypeObjects definedValues, DataTypeObjects definedVariables) {
        int lastIndex = 0;
        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);
            if (ch == '+') {
                if (!evaluateStatementObject(expression.substring(lastIndex, i))) {
                    return false;
                }
                lastIndex = i + 1;
            }
        }
        if (lastIndex == 0) {
            return evaluateStatementObject(expression) && areObjectsInDataType(getAllObjectsFromStatement(expression), definedValues, definedVariables);
        }
        return areObjectsInDataType(getAllObjectsFromStatement(expression), definedValues, definedVariables);
    }

    public static boolean isInterrogationValid(String interrogation) {
        int lastIndex = 0;
        for (int i = 0; i < interrogation.length(); i++) {
            char ch = interrogation.charAt(i);
            if (ch == '&') {
                if (i + 1 >= interrogation.length() && interrogation.charAt(i + 1) != '&') {
                    return false;
                }
                if (!evaluateInterrogationObject(interrogation.substring(lastIndex, i))) {
                    return false;
                }
                i++;
                lastIndex = i + 1;
            }
            if (ch == '|') {
                if (i + 1 >= interrogation.length() && interrogation.charAt(i + 1) != '|') {
                    return false;
                }
                if (!evaluateInterrogationObject(interrogation.substring(lastIndex, i))) {
                    return false;
                }
                i++;
                lastIndex = i + 1;
            }
        }
        if (lastIndex == 0) {
            return evaluateInterrogationObject(interrogation);
        }
        return true;
    }

    private static DataTypeObjects getAllObjectsFromInterrogation(String interrogation) {
        DataTypeObjects result = new DataTypeObjects();
        StringBuilder variableName = new StringBuilder();
        for (char ch : interrogation.toCharArray()) {
            if (isLetter(ch) || isNumber(ch)) {
                variableName.append(ch);
            } else {
                if (!variableName.toString().isEmpty()) {
                    result.add(variableName.toString());
                    variableName = new StringBuilder();
                }
            }
        }
        return result;
    }


    private static DataTypeObjects getAllObjectsFromStatement(String statement) {
        DataTypeObjects result = new DataTypeObjects();
        StringBuilder variableName = new StringBuilder();
        StringBuilder variableCount = new StringBuilder();
        boolean isName = false;
        for (char ch : statement.toCharArray()) {
            if (isLetter(ch) || isNumber(ch)) {
                if (isName) {
                    variableName.append(ch);
                } else {
                    variableCount.append(ch);
                }
            }
            if (String.valueOf(ch).equals("'")) {
                isName = true;
            }
            if (String.valueOf(ch).equals("+")) {
                isName = false;
                result.add(variableName.toString(), Integer.valueOf(variableCount.toString()));
                variableCount = new StringBuilder();
                variableName = new StringBuilder();
            }
        }
        if (!variableCount.toString().isEmpty() && !variableName.toString().isEmpty()) {
            result.add(variableName.toString(), Integer.valueOf(variableCount.toString()));
        }
        return result;
    }


    private static boolean evaluateStatementObject(String subExpression) {
        return subExpression.matches(STATEMENT_REGEX);
    }

    private static boolean evaluateInterrogationObject(String subInterrogation) {
        return subInterrogation.matches(LOWER_INTERROGATION_REGEX) ||
                subInterrogation.matches(LOWER_OR_EQUAL_INTERROGATION_REGEX) ||
                subInterrogation.matches(GREATER_INTERROGATION_REGEX) ||
                subInterrogation.matches(GREATER_OR_EQUAL_INTERROGATION_REGEX) ||
                subInterrogation.matches(EQUAL_INTERROGATION_REGEX) ||
                subInterrogation.matches(NOT_EQUAL_INTERROGATION_REGEX);
    }

    private static DataTypeObjects removeFromDefinedObjects(DataTypeObjects input, DataTypeObjects definedObjects) {
        Iterator it = input.getObjects().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry item = (Map.Entry) it.next();
            if (!definedObjects.getObjects().containsKey(item.getKey())) {
                it.remove();
            }
        }
        return input;
    }

    public static boolean statementHasVariables(String statement, DataTypeObjects variables) {
        DataTypeObjects extractedVariables = getSpecificObjectsFromStatement(statement, variables);
        return !extractedVariables.getObjects().isEmpty();
    }

    public static boolean statementHasConstants(String statement, DataTypeObjects values) {
        DataTypeObjects extractedValues = getSpecificObjectsFromStatement(statement, values);
        return !extractedValues.getObjects().isEmpty();
    }


    private static boolean isNumber(char ch) {
        return Character.isDigit(ch);
    }

    private static boolean isLetter(char ch) {
        return Character.isLetter(ch);
    }
}
