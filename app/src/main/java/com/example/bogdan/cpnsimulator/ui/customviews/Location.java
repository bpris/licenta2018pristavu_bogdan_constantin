package com.example.bogdan.cpnsimulator.ui.customviews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import com.example.bogdan.cpnsimulator.R;
import com.example.bogdan.cpnsimulator.data.DataTypeObjects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bogdan on 12.01.2018.
 */

public class Location extends BaseEntity {
    private String dataTypeName;
    private DataTypeObjects initialMarking;

    public static final String JSON_INITIAL_MARKING_KEY = "initial_marking";
    public static final String JSON_DATA_TYPE_NAME_KEY = "data_type_name";

    public Location(@NonNull Context context) {
        super(context);
        initialMarking = new DataTypeObjects();
    }

    public Location(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialMarking = new DataTypeObjects();
    }

    public Location(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialMarking = new DataTypeObjects();
    }

    public Location(Context context, String name) {
        super(context, name);
        initialMarking = new DataTypeObjects();
    }

    @Override
    protected void inflateLayout() {
        inflate(getContext(), R.layout.circle_view, this);
    }

    @Override
    public TextView getViewName() {
        return findViewById(R.id.state_name);
    }

    @Override
    public EditText getEditViewName() {
        return findViewById(R.id.edit_state_name);
    }

    @Override
    public void forceCloseAll() {
        super.forceCloseAll();
    }

    public String getDataTypeName() {
        return dataTypeName;
    }

    public void setDataTypeName(String dataTypeName) {
        this.dataTypeName = dataTypeName;
    }

    public DataTypeObjects getInitialMarking() {
        return initialMarking;
    }

    public void setInitialMarking(DataTypeObjects initialMarking) {
        this.initialMarking = initialMarking;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject object = new JSONObject();
        try {
            object.accumulate(JSON_NAME_KEY, getName());
            object.accumulate(JSON_X_COORDINATE, this.getX());
            object.accumulate(JSON_Y_COORDINATE, this.getY());
            if (this.dataTypeName != null) {
                object.accumulate(JSON_DATA_TYPE_NAME_KEY, this.dataTypeName);
            }
            if (this.getInitialMarking() != null) {
                object.accumulate(JSON_INITIAL_MARKING_KEY, new JSONObject(this.getInitialMarking().getObjects()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
